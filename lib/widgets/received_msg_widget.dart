import 'package:flutter/material.dart';

class ReceivedMsgWidget extends StatelessWidget {
  final String content;
  ReceivedMsgWidget({this.content});

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Padding(
        padding: EdgeInsets.only(right: 75.0,left: 8.0,top: 8.0,bottom: 8.0),
        child: ClipRRect(
          borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(0),
              bottomRight: Radius.circular(15),
              topLeft: Radius.circular(15),
              topRight: Radius.circular(15)
          ),
          child: Container(
            color: Color(0xFF71C7E3),
            child: Stack(
              children: [
                Padding(
                  padding:EdgeInsets.only(right: 12,left: 23,top: 8,bottom: 15),
                  child: Text(content,style: TextStyle(color: Color(0xFF003585)),),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
