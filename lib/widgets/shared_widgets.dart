import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_switch/flutter_switch.dart';

class MyButton extends StatelessWidget {
  String text;
  Function onPressed;
  Color color;

  MyButton({this.text, this.onPressed, this.color});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 131,
      height: 50,
      child: RaisedButton(
        onPressed: onPressed,
        child: Text(
          text,
          style: TextStyle(
              color: color, fontSize: 16, fontWeight: FontWeight.w800),
        ),
      ),
    );
  }
}

class CustomTextField extends StatelessWidget {
  String hinttext;
  bool isPassword;
  TextEditingController textEditingController;
  bool isNumber;
  Color borderColor;

  CustomTextField(
      {this.hinttext,
      this.isPassword,
      this.textEditingController,
      this.isNumber,
      this.borderColor});

  @override
  Widget build(BuildContext context) {
    return TextField(
      decoration: InputDecoration(
        enabled: true,
        enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(color: borderColor, width: 4),
            borderRadius: BorderRadius.all(Radius.circular(12))),
        focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(color: borderColor, width: 4),
            borderRadius: BorderRadius.all(Radius.circular(12))),
        contentPadding: EdgeInsets.fromLTRB(36, 24, 24, 24),
        hintText: hinttext,
        hintStyle: TextStyle(color: Colors.grey[700]),
      ),
      obscureText: isPassword ? true : false,
      controller: textEditingController,
      keyboardType: isNumber ? TextInputType.number : TextInputType.text,
    );
  }
}

class AuthTextField extends StatelessWidget {
  String hinttext;
  bool isNumber;
  Widget prefixIcon;
  bool isPassword;
  TextEditingController textEditingController;
  Widget suffixIcon;
  Color color;
  Function validator;
  Function onSaved;

  AuthTextField(
      {this.hinttext,
      this.isNumber,
      this.prefixIcon,
      this.isPassword,
      this.textEditingController,
      this.suffixIcon,
      this.color,
      this.validator,
      this.onSaved
      });

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      decoration: InputDecoration(
          suffixIcon: suffixIcon,
          enabled: true,
          enabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(width: 3, color: color)),
          prefixIcon: prefixIcon,
          hintText: hinttext),
      obscureText: isPassword ? true : false,
      controller: textEditingController,
      onSaved: onSaved,
      validator: validator,
      keyboardType: isNumber ? TextInputType.number : TextInputType.text,
    );
  }
}

class CustomDrawer extends StatefulWidget {
  @override
  _CustomDrawerState createState() => _CustomDrawerState();
}

class _CustomDrawerState extends State<CustomDrawer> {
  bool status = false;
  final _bigFont = const TextStyle(
      fontSize: 18, fontWeight: FontWeight.bold, color: Color(0xFF616161));

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: Theme.of(context).copyWith(canvasColor: Colors.yellow[100]),
      child: Drawer(
        child: ListView(
          children: [
            DrawerHeader(
              margin: EdgeInsets.all(0.0),
              decoration: BoxDecoration(color: Color(0xFFF7A74D)),
              child: Align(
                alignment: Alignment.center,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    GestureDetector(
                      onTap: () {
                        Navigator.pushNamed(context, '/user');
                      },
                      child: CircleAvatar(
                        radius: 36,
                      ),
                    ),
                    Text(
                      'Test',
                      style: TextStyle(
                          fontWeight: FontWeight.w800,
                          fontSize: 24,
                          color: Color(0xFF97080E)),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Text(
                          'Buyer',
                          style: TextStyle(
                              color: Color(0xFF97080E),
                              fontSize: 20,
                              fontWeight: FontWeight.bold),
                        ),
                        FlutterSwitch(
                            activeColor: Color(0xFF97080E),
                            inactiveColor: Color(0xFF97080E),
                            toggleColor: Color(0xFFF7A74D),
                            width: 130.0,
                            height: 25,
                            value: status,
                            onToggle: (val) {
                              setState(() {
                                status = val;
                                Navigator.pop(context);
                                Navigator.pushNamed(context, '/seller_login');
                              });
                            }),
                        Text(
                          'Seller',
                          style: TextStyle(
                              color: Color(0xFF97080E),
                              fontSize: 20,
                              fontWeight: FontWeight.bold),
                        )
                      ],
                    )
                  ],
                ),
              ),
            ),
            ListTile(
              title: Text(
                'Home',
                style: _bigFont,
              ),
              leading: Icon(Icons.home, size: 30),
              onTap: () {
                Navigator.pop(context);
                Navigator.pushNamed(context, '/home');
              },
            ),
            ListTile(
              title: Text(
                'Advance Search',
                style: _bigFont,
              ),
              leading: Icon(Icons.search, size: 30),
              onTap: () {
                Navigator.pop(context);
                Navigator.pushNamed(context, '/search');
              },
            ),
            ListTile(
              title: Text(
                'Login',
                style: _bigFont,
              ),
              leading: Icon(Icons.exit_to_app, size: 30),
              onTap: () {
                Navigator.pop(context);
                Navigator.pushNamed(context, '/login');
              },
            ),
            ListTile(
              title: Text(
                'Register',
                style: _bigFont,
              ),
              leading: Icon(Icons.person_outline, size: 30),
              onTap: () {
                Navigator.pop(context);
                Navigator.pushNamed(context, '/signup');
              },
            ),
            ListTile(
              title: Text(
                'Sellers',
                style: _bigFont,
              ),
              leading: Icon(Icons.account_circle_outlined, size: 30),
              onTap: () {
                Navigator.pop(context);
                Navigator.pushNamed(context, '/sellers');
              },
            ),
            ListTile(
              title: Text(
                'My Orders',
                style: _bigFont,
              ),
              leading: Icon(Icons.list, size: 30),
              onTap: () {
                Navigator.pop(context);
                Navigator.pushNamed(context, '/my_orders');
              },
            ),
            ListTile(
              title: Text(
                'Dispute',
                style: _bigFont,
              ),
              leading: Image.asset('assets/dispute.png', height: 32),
              onTap: () {
                Navigator.pop(context);
                Navigator.pushNamed(context, '/dispute');
              },
            ),
            Divider(color: Color(0xFFF7A74D), thickness: 3),
            Padding(
              padding: const EdgeInsets.only(left: 16.0),
              child: Text('Others',
                  style: TextStyle(
                      fontWeight: FontWeight.bold, color: Color(0xFFF7A74D))),
            ),
            ListTile(
              title: Text(
                'Settings',
                style: _bigFont,
              ),
              leading: Icon(Icons.settings_outlined, size: 30),
              onTap: () {
                Navigator.pop(context);
                Navigator.pushNamed(context, '/app_settings');
              },
            ),
            ListTile(
              title: Text(
                'Notifications',
                style: _bigFont,
              ),
              leading: Icon(Icons.notifications_active_outlined, size: 30),
              onTap: () {
                Navigator.pop(context);
                Navigator.pushNamed(context, '/notifications');
              },
            )
          ],
        ),
      ),
    );
  }
}

class CustomIconButton extends StatelessWidget {
  String hinttext;
  Function onPressed;
  Widget icon;
  Color textColor;

  CustomIconButton({this.hinttext, this.onPressed, this.icon,this.textColor});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 340,
      height: 50,
      child: RaisedButton.icon(
          onPressed: onPressed,
          icon: icon,
          label: Text(
            hinttext,
            style: TextStyle(
                color: textColor,
                fontSize: 24,
                fontWeight: FontWeight.w700),
          )),
    );
  }
}

class SuffixedIconTextField extends StatelessWidget {
  String hinttext;
  TextEditingController textEditingController;
  bool isNumber;
  Widget suffixIcon;
  Color fillColor;
  bool filled;
  TextAlign textAlign;
  Color borderColor;

  SuffixedIconTextField(
      {this.isNumber,
      this.suffixIcon,
      this.textEditingController,
      this.hinttext,
      this.fillColor,
      this.filled,
      this.textAlign,
      this.borderColor
      });

  @override
  Widget build(BuildContext context) {
    return TextField(
      style: TextStyle(
          fontSize: 20, fontWeight: FontWeight.w600, color: Colors.grey[700]),
      textAlign: textAlign,
      decoration: InputDecoration(
          filled: filled,
          fillColor: fillColor,
          contentPadding:
              EdgeInsets.only(top: 4, bottom: 4, right: 8, left: 18),
          suffixIcon: suffixIcon,
          enabled: true,
          enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(color: borderColor, width: 4),
              borderRadius: BorderRadius.all(Radius.circular(12))),
          focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(color: borderColor, width: 4),
              borderRadius: BorderRadius.all(Radius.circular(12))),
          hintText: hinttext,
          hintStyle: TextStyle(
              fontWeight: FontWeight.w600,
              fontSize: 18,
              color: Colors.grey[700])),
      controller: textEditingController,
      keyboardType: isNumber ? TextInputType.number : TextInputType.text,
    );
  }
}

Widget appBar(Color color) {
  return AppBar(
    title: Text('R2B',
        style:
            TextStyle(fontFamily: 'Rammetto One', color: color, fontSize: 24)),
    centerTitle: true,
    automaticallyImplyLeading: false,
  );
}

Widget CustomDropDown(dynamic value, Function onChanged,
    List<DropdownMenuItem<dynamic>> items, String hinttext) {
  return DropdownButtonFormField(
      decoration: InputDecoration(
        enabled: true,
        enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Color(0xFF97080E), width: 4),
            borderRadius: BorderRadius.all(Radius.circular(12))),
        focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Color(0xFF97080E), width: 4),
            borderRadius: BorderRadius.all(Radius.circular(12))),
        contentPadding: EdgeInsets.fromLTRB(36, 24, 24, 24),
      ),
      icon: Icon(
        Icons.keyboard_arrow_down,
        color: Color(0xFF97080E),
      ),
      value: value,
      items: items,
      hint: Text(
        hinttext,
        style:
            TextStyle(fontWeight: FontWeight.w600, fontSize: 20, height: 1.1),
      ),
      onChanged: onChanged);
}

class BaseAppBar extends StatelessWidget implements PreferredSizeWidget {
  final title;
  final AppBar appBar;
  final List<Widget> widgets;
  final bool automaticallyImplyLeading;
  final bool isCentered;
  final Widget leading;
  final Color iconTheme;
  final Color titleColor;

  const BaseAppBar(
      {Key key,
      this.title,
      this.appBar,
      this.widgets,
      this.automaticallyImplyLeading,
      this.isCentered,
      this.leading,
      this.iconTheme,
      this.titleColor
      })
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      leading: leading,
      centerTitle: isCentered,
      automaticallyImplyLeading: automaticallyImplyLeading,
      iconTheme: IconThemeData(color: iconTheme),
      textTheme: TextTheme(),
      title: Text(
        title,
        style: TextStyle(
            fontSize: 24,
            fontWeight: FontWeight.bold,
            color: titleColor),
      ),
      actions: widgets,
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(appBar.preferredSize.height);
}

class SellerDrawer extends StatefulWidget {
  @override
  _SellerDrawerState createState() => _SellerDrawerState();
}

class _SellerDrawerState extends State<SellerDrawer> {
  bool status = true;
  final _bigFont = const TextStyle(
      fontSize: 18, fontWeight: FontWeight.bold, color: Color(0xFF616161));

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: [
          DrawerHeader(
            margin: EdgeInsets.all(0.0),
            decoration: BoxDecoration(color: Color(0xFF71C7E3)),
            child: Align(
              alignment: Alignment.center,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  GestureDetector(
                    onTap: () {
                      Navigator.pushNamed(context, '/seller_profile');
                    },
                    child: CircleAvatar(
                      radius: 36,
                    ),
                  ),
                  Text(
                    'Test',
                    style: TextStyle(
                        fontWeight: FontWeight.w800,
                        fontSize: 24,
                        color: Color(0xFF003585)),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Text(
                        'Buyer',
                        style: TextStyle(
                            color: Color(0xFF003585),
                            fontSize: 20,
                            fontWeight: FontWeight.bold),
                      ),
                      FlutterSwitch(
                          activeColor: Color(0xFF003585),
                          inactiveColor: Color(0xFF003585),
                          toggleColor: Colors.white,
                          width: 130.0,
                          height: 25,
                          value: status,
                          onToggle: (val) {
                            setState(() {
                              status = val;
                              Navigator.pop(context);
                              Navigator.pushNamed(context, '/login');
                            });
                          }),
                      Text(
                        'Seller',
                        style: TextStyle(
                            color: Color(0xFF003585),
                            fontSize: 20,
                            fontWeight: FontWeight.bold),
                      )
                    ],
                  )
                ],
              ),
            ),
          ),
          ListTile(
            title: Text(
              'Home',
              style: _bigFont,
            ),
            leading: Icon(Icons.home, size: 30),
            onTap: () {
              Navigator.pop(context);
              Navigator.pushNamed(context, '/seller_home');
            },
          ),
          ListTile(
            title: Text(
              'Package',
              style: _bigFont,
            ),
            leading: Image.asset('assets/tag.png', height: 30),
            onTap: () {

            },
          ),
          ListTile(
            title: Text(
              'Login',
              style: _bigFont,
            ),
            leading: Icon(Icons.exit_to_app, size: 30),
            onTap: () {
              Navigator.pop(context);
              Navigator.pushNamed(context, '/seller_login');
            },
          ),
          ListTile(
            title: Text(
              'Register',
              style: _bigFont,
            ),
            leading: Icon(Icons.person_outline, size: 30),
            onTap: () {
              Navigator.pop(context);
              Navigator.pushNamed(context, '/seller_signup');
            },
          ),
          ListTile(
            title: Text(
              'Chat',
              style: _bigFont,
            ),
            leading: Icon(Icons.message_outlined, size: 30),
            onTap: () {
              Navigator.pop(context);
              Navigator.pushNamed(context, '/seller_chats');
            },
          ),
          ListTile(
            title: Text(
              'All Listing',
              style: _bigFont,
            ),
            leading: Icon(Icons.description_outlined, size: 30),
            onTap: () {
              Navigator.pop(context);
              Navigator.pushNamed(context, '/all_listing');
            },
          ),
          ListTile(
            title: Text(
              'Order',
              style: _bigFont,
            ),
            leading: Image.asset('assets/list.png', height: 32),
            onTap: () {
              Navigator.pop(context);
              Navigator.pushNamed(context, '/seller_order');
            },
          ),
          ListTile(
            title: Text(
              'Dispute',
              style: _bigFont,
            ),
            leading: Image.asset('assets/dispute.png', height: 32),
            onTap: () {
              Navigator.pop(context);
              Navigator.pushNamed(context, '/seller_dispute');
            },
          ),
          Divider(color: Color(0xFF71C7E3), thickness: 3),
          Padding(
            padding: const EdgeInsets.only(left: 16.0),
            child: Text('Others',
                style: TextStyle(
                    fontWeight: FontWeight.bold, color: Color(0xFF71C7E3))),
          ),
          ListTile(
            title: Text(
              'Settings',
              style: _bigFont,
            ),
            leading: Icon(Icons.settings_outlined, size: 30),
            onTap: () {
              Navigator.pop(context);
              Navigator.pushNamed(context, '/seller_app_settings');
            },
          ),
          ListTile(
            title: Text(
              'Notifications',
              style: _bigFont,
            ),
            leading: Icon(Icons.notifications_active_outlined, size: 30),
            onTap: () {
              Navigator.pop(context);
              Navigator.pushNamed(context, '/seller_notifications');
            },
          )
        ],
      ),
    );
  }
}


class CustomDivider extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Divider(
      color: Color(0xFF71C7E3),
      height: 5.0,
      thickness: 3,
    );
  }
}