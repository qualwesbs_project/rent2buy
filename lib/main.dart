import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:rent2buy/screens/buyer/login.dart';
import 'package:rent2buy/screens/buyer/signup.dart';
import 'package:rent2buy/screens/buyer/categories.dart';
import 'package:rent2buy/screens/buyer/chat_screen.dart';
import 'package:rent2buy/screens/buyer/chats.dart';
import 'package:rent2buy/screens/buyer/confirmed_screen.dart';
import 'package:rent2buy/screens/buyer/dispute.dart';
import 'package:rent2buy/screens/buyer/first_screen.dart';
import 'package:rent2buy/screens/buyer/forget_password.dart';
import 'package:rent2buy/screens/buyer/home.dart';
import 'package:rent2buy/screens/buyer/image_slider.dart';
import 'package:rent2buy/screens/buyer/my_cart.dart';
import 'package:rent2buy/screens/buyer/my_orders.dart';
import 'package:rent2buy/screens/buyer/otp_verify.dart';
import 'package:rent2buy/screens/buyer/payable.dart';
import 'package:rent2buy/screens/buyer/product_detail.dart';
import 'package:rent2buy/screens/buyer/profile.dart';
import 'package:rent2buy/screens/buyer/qr_code.dart';
import 'package:rent2buy/screens/buyer/rent_confirm_details.dart';
import 'package:rent2buy/screens/buyer/reset_password.dart';
import 'package:rent2buy/screens/buyer/search.dart';
import 'package:rent2buy/screens/buyer/search_for_rent.dart';
import 'package:rent2buy/screens/buyer/search_results.dart';
import 'package:rent2buy/screens/seller/add_listing.dart';
import 'package:rent2buy/screens/seller/all_listing.dart';
import 'package:rent2buy/screens/seller/app_settings.dart';
import 'package:rent2buy/screens/seller/bank_account_details.dart';
import 'package:rent2buy/screens/seller/chat_screen.dart';
import 'package:rent2buy/screens/seller/chats.dart';
import 'package:rent2buy/screens/seller/dispute.dart';
import 'package:rent2buy/screens/seller/forget_password.dart';
import 'package:rent2buy/screens/seller/home.dart';
import 'package:rent2buy/screens/seller/item_add.dart';
import 'package:rent2buy/screens/seller/login.dart';
import 'package:rent2buy/screens/seller/notifications.dart';
import 'package:rent2buy/screens/seller/order.dart';
import 'package:rent2buy/screens/seller/otp_verify.dart';
import 'package:rent2buy/screens/seller/product_detail.dart';
import 'package:rent2buy/screens/seller/profile.dart';
import 'package:rent2buy/screens/seller/reset_password.dart';
import 'package:rent2buy/screens/seller/seller_settings.dart';
import 'package:rent2buy/screens/seller/selling_report.dart';
import 'package:rent2buy/screens/seller/signup.dart';
import 'package:rent2buy/screens/buyer/sellers.dart';
import 'package:rent2buy/screens/buyer/set_location.dart';
import 'package:rent2buy/screens/splashscreen.dart';
import 'package:rent2buy/screens/buyer/user.dart';
import 'package:rent2buy/screens/buyer/app_settings.dart';
import 'package:rent2buy/screens/buyer/notifications.dart';
Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {

    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
          fontFamily: 'Dosis',
          scaffoldBackgroundColor: Colors.yellow[100],
          primaryColor: Color(0xFFF7A74D),
          buttonTheme: ButtonThemeData(buttonColor: Color(0xFFF7A74D)),
          appBarTheme:AppBarTheme(iconTheme: IconThemeData(color: Color(0xFF97080E)))
      ),
      routes: {
        '/login': (_) => Login(),
        '/signup': (_) => SignUp(),
        '/forget_pwd': (_) => ForgetPassword(),
        '/otp_verify': (_) => OtpVerify(),
        '/reset_pwd':(_) => ResetPassword(),
        '/set_location':(_) => SetLocation(),
        '/home':(_) => Home(),
        '/profile':(_) => Profile(),
        '/user': (_) => User(),
        '/first_screen': (_) => FirstScreen(),
        '/search_for_rent':(_) => SearchForRent(),
        '/search':(_) => Search(),
        '/app_settings':(_) => AppSettings(),
        '/notifications':(_) => Notifications(),
        '/categories':(_) => Categories(),
        '/confirmed_screen':(_) => ConfirmedScreen(),
        '/dispute':(_) => Dispute(),
        '/my_orders':(_) => MyOrders(),
        '/chats':(_) => Chats(),
        '/sellers':(_) => Sellers(),
        '/search_results':(_) => SearchResults(),
        '/product_detail':(_)=> ProductDetails(),
        '/my_cart':(_) => MyCart(),
        '/image_slider':(_) => ImageSlider(),
        '/qr_code':(_) => Qrcode(),
        '/payable':(_) => Payable(),
        '/rent_confirmation':(_) => RentConfirmation(),
        '/chat_screen':(_) => ChatScreen(),
        '/seller_login':(_) => SellerLogin(),
        '/seller_signup':(_) => SellerSignup(),
        '/seller_home':(_) => SellerHome(),
        '/seller_forget_pwd':(_) => SellerForgetPassword(),
        '/seller_OTP_verify':(_) => SellerOTPVerify(),
        '/seller_reset_pwd':(_) => SellerResetPassword(),
        '/seller_app_settings':(_) => SellerAppSettings(),
        '/seller_notifications':(_) => SellerNotifications(),
        '/seller_chats':(_) =>SellerChats(),
        '/seller_profile':(_) => SellerProfile(),
        '/seller_settings':(_) => SellerSettings(),
        '/selling_report':(_) => SellingReport(),
        '/add_listing':(_) => AddListing(),
        '/bank_account':(_) =>BankAccount(),
        '/seller_dispute':(_) => SellerDispute(),
        '/seller_order':(_) => SellerOrder(),
        '/item_added':(_) => ItemAdded(),
        '/seller_product_detail':(_) => SellerProductDetail(),
        '/all_listing':(_) => AllListing(),
        '/seller_chat_screen':(_) => SellerChatScreen()
      },
      home: SplashScreen(),
    );
  }
}
