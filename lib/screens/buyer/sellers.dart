import 'package:flutter/material.dart';
import 'package:rent2buy/models/seller_model.dart';
import 'package:rent2buy/widgets/shared_widgets.dart';
import 'package:smooth_star_rating/smooth_star_rating.dart';

class Sellers extends StatelessWidget {

  List<SellerModel> _seller =[
    SellerModel(name: 'Seller 1',location: 'Location ',rating: 5,imageUrl: 'https://www.clipartmax.com/png/middle/34-340027_user-login-man-human-body-mobile-person-comments-person-icon-png.png'),
    SellerModel(name: 'Seller 2',location: 'Location ',rating: 4,imageUrl: 'https://www.clipartmax.com/png/middle/34-340027_user-login-man-human-body-mobile-person-comments-person-icon-png.png'),
    SellerModel(name: 'Seller 3',location: 'Location ',rating: 3,imageUrl: 'https://www.clipartmax.com/png/middle/34-340027_user-login-man-human-body-mobile-person-comments-person-icon-png.png'),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: BaseAppBar(
        title: 'Sellers',
        automaticallyImplyLeading: true,
        titleColor: Color(0xFF97080E),
        iconTheme: Color(0xFF97080E),
        widgets: [],
        appBar: AppBar(),
      ),
      body: Container(
        padding: EdgeInsets.symmetric(vertical: 10,horizontal: 10),
        child: ListView.builder(itemCount: _seller.length,itemBuilder: (BuildContext context, int index){
            return Container(
              height: 150,
              child: Card(
                elevation: 8,
                color: Colors.yellow[50],
                child: Row(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Image.network(_seller[index].imageUrl,width: 100,height: 100,),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(16.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children:[
                          Text(_seller[index].name,style: TextStyle(color: Color(0xFF97080E),fontWeight: FontWeight.w700,fontSize: 24)),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Icon(Icons.location_on,color: Color(0xFFF7A74D),),
                              Text(_seller[index].location,style: TextStyle(color:Colors.grey,fontSize: 16),)
                            ],
                          ),
                          SmoothStarRating(
                            allowHalfRating: false,
                            starCount: 5,
                            rating: _seller[index].rating,
                            color: Color(0xFF97080E),
                            borderColor: Color(0xFF97080E),
                          )
                        ]
                      ),
                    )
                  ],
                ),
              ),
            );
        }),
      )
    );
  }
}
