import 'package:flutter/material.dart';
import 'package:rent2buy/widgets/shared_widgets.dart';
import 'package:rent2buy/data.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          iconTheme: IconThemeData(color: Color(0xFF97080E)),
          title: Text(
            'Rent 2 Buy',
            style: TextStyle(
                fontSize: 24,
                fontWeight: FontWeight.bold,
                color: Color(0xFF97080E)),
          ),
          actions: [
            IconButton(
                icon: Icon(
                  Icons.search,
                ),
                onPressed: () {
                  Navigator.pushNamed(context, '/search');
                }),
            IconButton(
                icon: Icon(
                  Icons.location_pin,
                ),
                onPressed: () {
                  Navigator.pushNamed(context, '/set_location');
                })
          ],
        ),
        drawer: CustomDrawer(),
        body: SingleChildScrollView(
          child: Container(
            width: MediaQuery.of(context).size.width,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Stack(
                  children: [
                    Image.network(
                      'https://www.breathintravel.com/wp-content/uploads/2020/09/best-backpacking-binoculars-1150x766.jpg',
                      width: MediaQuery.of(context).size.width,
                    ),
                    Positioned(
                      top: 30,
                      left: 20,
                      child: Text(
                        'Search from \nThousands of \nAds',
                        style: TextStyle(
                            color: Color(0xFF97080E),
                            fontWeight: FontWeight.w700,
                            fontSize: 30),
                      ),
                    ),
                    Positioned(
                      top: 150,
                      left: 20,
                      child: Text(
                        'Here you can Search',
                        style: TextStyle(
                            color: Color(0xFF97080E),
                            fontWeight: FontWeight.w700,
                            fontSize: 16),
                      ),
                    ),
                    Positioned(
                      top: 180,
                      width: MediaQuery.of(context).size.width,
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 20.0),
                        child: SuffixedIconTextField(
                            filled: true,
                            fillColor: Colors.white54,
                            borderColor: Color(0xFF97080E),
                            hinttext: 'Search by Keyword',
                            textAlign: TextAlign.justify,
                            isNumber: false,
                            suffixIcon: IconButton(
                                icon: Icon(
                                  Icons.search,
                                  color: Color(0xFF97080E),
                                ),
                                onPressed: () {})),
                      ),
                    )
                  ],
                ),
                Container(
                  padding: EdgeInsets.all(16),
                  child: GridView.builder(
                      physics: ScrollPhysics(),
                      scrollDirection: Axis.vertical,
                      shrinkWrap: true,
                      itemCount: images.length,
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: 3,
                          crossAxisSpacing: 6,
                          mainAxisSpacing: 10),
                      itemBuilder: (BuildContext context, int index) {
                        return GestureDetector(
                          onTap: (){
                            Navigator.pushNamed(
                                context, '/product_detail');
                          },
                          child: Container(
                              decoration: BoxDecoration(
                                  border: Border.all(
                                      width: 3, color: Color(0xFFF7A74D))),
                              child: Image.network(
                                images[index],
                                fit: BoxFit.contain,
                              )),
                        );
                      }),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 12.0),
                  child: SizedBox(
                      width: 170,
                      child: MyButton(
                          text: 'View All Categories',
                          color: Color(0xFF97080E),
                          onPressed: () {
                            Navigator.pushNamed(context, '/categories');
                          })),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
