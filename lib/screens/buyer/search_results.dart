import 'package:flutter/material.dart';
import 'package:rent2buy/widgets/shared_widgets.dart';
import 'package:rent2buy/data.dart';

class SearchResults extends StatefulWidget {
  @override
  _SearchResultsState createState() => _SearchResultsState();
}

class _SearchResultsState extends State<SearchResults> {
  String filter = 'Newest First';
  String priceFilter = 'Any';
  int _selectedIndex = 2;

  static const _filters = <String>['Newest First', 'Oldest First'];
  static const _priceFilters = <String>['Any', 'Highest', 'Lowest'];

  final List<DropdownMenuItem<String>> _dropDownMenuFilter = _filters
      .map(
        (String value) =>
        DropdownMenuItem<String>(
          value: value,
          child: Text(value,
              style: TextStyle(fontWeight: FontWeight.w600, fontSize: 18)),
        ),
  )
      .toList();
  final List<DropdownMenuItem<String>> _dropDownMenuPriceFilter = _priceFilters
      .map(
        (String value) =>
        DropdownMenuItem<String>(
          value: value,
          child: Text(value,
              style: TextStyle(fontWeight: FontWeight.w600, fontSize: 18)),
        ),
  )
      .toList();

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: BaseAppBar(
        leading: IconButton(icon: Icon(Icons.search), onPressed: () {
          Navigator.pushNamed(context, '/search');
        }),
        title: 'Rent 2 Buy',
        titleColor: Color(0xFF97080E),
        iconTheme: Color(0xFF97080E),
        isCentered: false,
        appBar: AppBar(),
        automaticallyImplyLeading: false,
        widgets: [
          IconButton(icon: Icon(Icons.dashboard_rounded), onPressed: () {
            Navigator.pushNamed(context, '/home');
          }),
          IconButton(icon: Icon(Icons.location_on), onPressed: () {
            Navigator.pushNamed(context, '/set_location');
          })
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              height: 60,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  FlatButton.icon(
                    onPressed: () {
                      Navigator.pushNamed(context, '/rent_confirmation');
                    },
                    icon: Icon(
                      Icons.location_on,
                      color: Color(0xFF97080E),
                    ),
                    label: Text(
                      'Pick Up',
                      style: TextStyle(
                          color: Color(0xFF97080E),
                          fontWeight: FontWeight.w600,
                          fontSize: 18),
                    ),
                  ),
                  VerticalDivider(thickness: 2),
                  FlatButton.icon(
                    onPressed: () {},
                    icon: Icon(
                      Icons.local_library_outlined,
                      color: Color(0xFF97080E),
                    ),
                    label: Text(
                      'Shipping',
                      style: TextStyle(
                          color: Color(0xFF97080E),
                          fontWeight: FontWeight.w600,
                          fontSize: 18),
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  DropdownButton(
                      value: filter,
                      items: _dropDownMenuFilter,
                      onChanged: (String newVal) {
                        setState(() {
                          filter = newVal;
                        });
                      }),
                  Row(
                    children: [
                      Text('Price: ',
                          style: TextStyle(
                              fontSize: 18, fontWeight: FontWeight.w600)),
                      DropdownButton(
                          value: priceFilter,
                          items: _dropDownMenuPriceFilter,
                          onChanged: (String newVal) {
                            setState(() {
                              priceFilter = newVal;
                            });
                          }),
                    ],
                  )
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.fromLTRB(16,6,16,16),
              child: GridView.builder(
                  physics: ScrollPhysics(),
                  scrollDirection: Axis.vertical,
                  shrinkWrap: true,
                  itemCount: images.length,
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2,
                      crossAxisSpacing: 6,
                      mainAxisSpacing: 10),
                  itemBuilder: (BuildContext context, int index) {
                    return Image.network(
                      images[index],
                      fit: BoxFit.contain,
                    );
                  }),
            ),
          ],
        ),
      ),
      bottomNavigationBar: BottomNavigationBar(
          type: BottomNavigationBarType.shifting,
          backgroundColor: Colors.yellow[100],
          selectedItemColor: Color(0xFF97080E),
          unselectedItemColor: Colors.grey,
          iconSize: 36,
          onTap: _onItemTapped,
          currentIndex: _selectedIndex,
          items: [
            BottomNavigationBarItem(backgroundColor: Colors.yellow[100],
              label: '',icon: Icon(Icons.home_outlined,),
            ),
            BottomNavigationBarItem(backgroundColor: Colors.yellow[100],
                label: '', icon: Icon(Icons.message_outlined)),
            BottomNavigationBarItem(backgroundColor: Colors.yellow[100],
                label: '', icon: Icon(Icons.camera_alt_outlined)),
            BottomNavigationBarItem(backgroundColor: Colors.yellow[100],
                label: '', icon: Icon(Icons.local_offer_outlined)),
            BottomNavigationBarItem(backgroundColor: Colors.yellow[100],
                label: '', icon: Icon(Icons.person_outline)),
          ]),
    );
  }
}
