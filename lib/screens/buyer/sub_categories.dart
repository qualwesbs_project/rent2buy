import 'package:flutter/material.dart';
import 'package:rent2buy/models/category_model.dart';
import 'package:rent2buy/widgets/shared_widgets.dart';
class SubCategory extends StatelessWidget {

  final CategoryModel categoryModel;
  SubCategory({Key key,@required this.categoryModel}): super(key:key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: BaseAppBar(
        title: categoryModel.category,
        automaticallyImplyLeading: true,
        titleColor: Color(0xFF97080E),
        iconTheme: Color(0xFF97080E),
        widgets: [],
        appBar: AppBar(),
      ),
      body: ListView.builder(
          itemCount: categoryModel.subcategories.length,
          itemBuilder: (BuildContext context, int index) {
            return Column(
              children: [
                ListTile(
                  contentPadding: EdgeInsets.symmetric(horizontal: 50),
                  title: Text(
                    categoryModel.subcategories[index],
                    style: TextStyle(fontWeight: FontWeight.w600, fontSize: 20),
                  ),
                  //onTap: (){Navigator.pushNamed(context, '/${_routes[index]}');},
//                    onTap: (){
//                      Navigator.push(context, MaterialPageRoute(
//                          builder: (context) =>
//                              SubCategoryResults(categoryModel: categoryModel.subcategories[index])));
//                    },
                ),
                Divider(
                  color: Color(0xFFF7A74D),
                  height: 5.0,
                  thickness: 3,
                ),
                
              ],
            );
          }),
    );
  }
}
