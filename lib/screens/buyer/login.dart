import 'dart:ui';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:rent2buy/widgets/shared_widgets.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  String _email;
  String _password;
  final _key = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          height: MediaQuery.of(context).size.height,
          child: Column(
            children: [
              Container(
                height: 200.0,
                color: Color(0xFFF7A74D),
                child: Center(
                  child: Text(
                    "Rent 2 Buy",
                    style: TextStyle(
                        fontFamily: 'Interval Sans Pro',
                        color: Color(0xFF97080E),
                        fontWeight: FontWeight.bold,
                        fontSize: 50.0,
                        shadows: [
                          Shadow(
                              color: Colors.white,
                              blurRadius: 1.0,
                              offset: Offset(-2.0, 2.0))
                        ]),
                  ),
                ),
              ),
              SizedBox(height: 20),
              Text(
                "Welcome Back!",
                style: TextStyle(fontSize: 40, fontWeight: FontWeight.w800),
              ),
              SizedBox(height: 30),
              Form(
                key: _key,
                child: Padding(
                  padding: const EdgeInsets.only(left: 20.0, right: 20),
                  child: Column(
                    children: [
                      AuthTextField(
                        hinttext: 'Email Address',
                        prefixIcon: Icon(Icons.email_outlined),
                        isPassword: false,
                        isNumber: false,
                        color: Color(0xFFF7A74D),
                        validator: (String value) {
                          Pattern pattern =  r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
                          RegExp regexp = RegExp(pattern);
                          if (value.isEmpty) {
                            return 'Please enter email';
                          }else if(!regexp.hasMatch(value)){
                            return 'Please enter valid Email';
                          }
                          return null;
                        },
                        onSaved: (String value) {
                          _email = value;
                        },
                      ),
                      SizedBox(height: 10),
                      AuthTextField(
                        hinttext: 'Password',
                        prefixIcon: Icon(Icons.lock_outline),
                        isPassword: true,
                        isNumber: false,
                        color: Color(0xFFF7A74D),
                        validator: (String value) {
                          if (value.isEmpty) {
                            return 'Please enter password';
                          } else if (value.length < 7) {
                            return 'Password should be greater than 8';
                          }
                          return null;
                        },
                        onSaved: (String value) {
                          _password = value;
                        },
                      )
                    ],
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(right: 20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    FlatButton(
                        onPressed: () {
                          Navigator.pushNamed(context, "/forget_pwd");
                        },
                        child: Text(
                          'Forget Password?',
                          style: TextStyle(color: Colors.grey[500]),
                        )),
                  ],
                ),
              ),
              MyButton(
                  text: "Submit",
                  color: Color(0xFF97080E),
                  onPressed: () {
                    if (_key.currentState.validate()) {
                      _key.currentState.save();
                      FirebaseAuth.instance
                          .signInWithEmailAndPassword(
                              email: _email, password: _password)
                          .then(
                              (value) => Navigator.of(context).pushNamedAndRemoveUntil('/home', (Route<dynamic> route) => false));
                    }
                  }),
              SizedBox(height: 20),
              Text('OR', style: TextStyle(fontWeight: FontWeight.bold)),
              SizedBox(height: 20),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  RawMaterialButton(
                      child: FaIcon(FontAwesomeIcons.facebookF,
                          size: 24, color: Colors.white),
                      fillColor: Colors.blue[900],
                      shape: CircleBorder(),
                      onPressed: () {}),
                  SizedBox(width: 20),
                  RawMaterialButton(
                      child: FaIcon(FontAwesomeIcons.googlePlusG,
                          size: 24, color: Colors.white),
                      fillColor: Colors.red,
                      shape: CircleBorder(),
                      onPressed: () {}),
                ],
              ),
              SizedBox(height: 20),
              MyButton(
                text: 'Guest Login',
                color: Color(0xFF97080E),
                onPressed: () {
                  Navigator.pushNamed(context, '/home');
                },
              ),
              Spacer(),
              Row(
                children: [
                  Expanded(
                    child: Divider(
                      color: Color(0xFFF7A74D),
                      height: 5.0,
                      thickness: 5,
                    ),
                  ),
                ],
              ),
              SizedBox(height: 10),
              Align(
                alignment: Alignment.bottomCenter,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(bottom: 20.0),
                      child: RichText(
                        text: TextSpan(
                          children: <TextSpan>[
                            TextSpan(
                                text: 'Not a Member Yet? ',
                                style: TextStyle(
                                    fontSize: 18,
                                    fontFamily: 'Dosis',
                                    color: Color(0xFF97080E),
                                    fontWeight: FontWeight.bold)),
                            TextSpan(
                                text: 'Register with us',
                                style: TextStyle(
                                    fontFamily: 'Dosis',
                                    fontSize: 18,
                                    color: Color(0xFFF7A74D),
                                    fontWeight: FontWeight.bold),
                                recognizer: TapGestureRecognizer()
                                  ..onTap = () {
                                    Navigator.pushNamed(context, '/signup');
                                  }),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
