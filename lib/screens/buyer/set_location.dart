import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:rent2buy/widgets/shared_widgets.dart';

class SetLocation extends StatefulWidget {
  @override
  _SetLocationState createState() => _SetLocationState();
}

class _SetLocationState extends State<SetLocation> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomPadding: true,
          appBar: AppBar(
            title: Text(
              'Set Location',
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: Color(0xFF97080E),
                  fontSize: 24),
            ),
            centerTitle: true,
            actions: [
              FlatButton(
                child: Text(
                  'Cancel',
                  style: TextStyle(color: Color(0xFF97080E)),
                ),
                onPressed: () {},
              )
            ],
            automaticallyImplyLeading: false,
          ),
          body: Container(
            padding: EdgeInsets.symmetric(horizontal: 60),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(height: 80),
                Text('Where you are Searching?',
                    style:
                        TextStyle(fontWeight: FontWeight.w700, fontSize: 20)),
                SizedBox(height: 20),
                CustomIconButton(
                  textColor: Color(0xFF97080E),
                  hinttext: 'Get my Location',
                  icon: Icon(Icons.location_pin, color: Color(0xFF97080E)),
                  onPressed: () {},
                ),
                SizedBox(height: 30),
                Text('OR',
                    style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w700,
                        color: Colors.grey[700])),
                SizedBox(height: 30),
                SuffixedIconTextField(
                    hinttext: 'Enter Location',
                    textAlign: TextAlign.center,
                    borderColor: Color(0xFF97080E),

                    suffixIcon: Icon(Icons.edit),
                    isNumber: false),
                SizedBox(height: 20),
                SuffixedIconTextField(
                    hinttext: 'Zip Code',
                    textAlign: TextAlign.center,
                    borderColor: Color(0xFF97080E),
                    suffixIcon: Icon(Icons.edit),
                    isNumber: true),
                Spacer(),
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: MyButton(text: 'Save Location', color: Color(0xFF97080E),onPressed: () {}),
                ),
              ],
            ),
          )),
    );
  }
}
