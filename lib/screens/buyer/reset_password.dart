import 'package:flutter/material.dart';
import 'package:rent2buy/widgets/shared_widgets.dart';

class ResetPassword extends StatefulWidget {
  @override
  _ResetPasswordState createState() => _ResetPasswordState();
}

class _ResetPasswordState extends State<ResetPassword> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: appBar(Color(0xFF97080E)),
        body: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 40),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: 60),
                Padding(
                  padding: EdgeInsets.only(left: 12.0),
                  child: Text('Reset Password',
                      style: TextStyle(
                          fontFamily: 'Dosis',
                          fontWeight: FontWeight.w700,
                          fontSize: 24)),
                ),
                SizedBox(height: 40),
                CustomTextField(
                    hinttext: 'Current Password',
                    borderColor: Color(0xFF97080E),
                    isPassword: true,
                    isNumber: false),
                SizedBox(height: 40),
                CustomTextField(
                    hinttext: 'New Password',
                    borderColor: Color(0xFF97080E),
                    isPassword: true,
                    isNumber: false),
                SizedBox(height: 40),
                CustomTextField(
                    hinttext: 'Confirm Password',
                    borderColor: Color(0xFF97080E),
                    isPassword: true,
                    isNumber: false),
                SizedBox(height: 60),
                Center(
                    child: MyButton(
                        text: 'Reset',
                        color: Color(0xFF97080E),
                        onPressed: () {
                          Navigator.pushNamed(context, '/set_location');
                        }))
              ],
            ),
          ),
        ),
      ),
    );
  }
}
