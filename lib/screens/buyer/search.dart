import 'package:flutter/material.dart';
import 'package:rent2buy/widgets/shared_widgets.dart';
import 'package:syncfusion_flutter_sliders/sliders.dart';
import 'package:syncfusion_flutter_core/theme.dart';

class Search extends StatefulWidget {
  @override
  _SearchState createState() => _SearchState();
}

class _SearchState extends State<Search> {
  final _bigFont = const TextStyle(
      fontWeight: FontWeight.w700, fontSize: 24, color: Color(0xff757575));
  String _selectedcategory;
  String _selectedtype;
  String _selectedlocation;
  double _value = 0.0;

  static const _categories = <String>[
    'Electronics',
    'Home Decor',
    'TV & Appliances',
    'Fashion',
    'Camera',
    'Sports & more',
    'Musical Instruments',
    'Cars',
    'Bikes',
    'Bicycle',
    'House',
    'Apartment',
    'Clothing',
    'Costumes',
    'Accessories'
  ];
  static const _type = <String>[
    'Rent',
    'Buy',
  ];
  static const _location = <String>[
    'India',
    'USA',
    'Australia',
  ];
  final List<DropdownMenuItem<String>> _dropDownMenuItem = _categories
      .map(
        (String value) => DropdownMenuItem<String>(
          value: value,
          child: Text(value,
              style: TextStyle(fontWeight: FontWeight.w600, fontSize: 20)),
        ),
      )
      .toList();
  final List<DropdownMenuItem<String>> _dropDownMenuItemType = _type
      .map(
        (String value) => DropdownMenuItem<String>(
          value: value,
          child: Text(value,
              style: TextStyle(fontWeight: FontWeight.w600, fontSize: 20)),
        ),
      )
      .toList();
  final List<DropdownMenuItem<String>> _dropDownMenuItemLocation = _location
      .map(
        (String value) => DropdownMenuItem<String>(
          value: value,
          child: Text(value,
              style: TextStyle(fontWeight: FontWeight.w600, fontSize: 20)),
        ),
      )
      .toList();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: BaseAppBar(
        titleColor: Color(0xFF97080E),
        iconTheme: Color(0xFF97080E),
        title: 'Search Here',
        automaticallyImplyLeading: true,
        appBar: AppBar(),
        widgets: [
          Padding(
            padding: const EdgeInsets.only(right:12.0),
            child: Icon(Icons.refresh),
          )
        ],
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 30),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: 30),
              CustomTextField(
                  hinttext: 'Search',borderColor: Color(0xFF97080E), isPassword: false, isNumber: false),
              SizedBox(height: 20),
              Padding(
                padding: EdgeInsets.only(left: 12.0),
                child: Text(
                  'Categories',
                  style: _bigFont,
                ),
              ),
              DropdownButtonFormField(
                decoration: InputDecoration(
                  enabled: true,
                  enabledBorder: OutlineInputBorder(
                      borderSide:
                          BorderSide(color: Color(0xFF97080E), width: 4),
                      borderRadius: BorderRadius.all(Radius.circular(12))),
                  focusedBorder: OutlineInputBorder(
                      borderSide:
                          BorderSide(color: Color(0xFF97080E), width: 4),
                      borderRadius: BorderRadius.all(Radius.circular(12))),
                  contentPadding: EdgeInsets.fromLTRB(36, 24, 24, 24),
                ),
                icon: Icon(
                  Icons.keyboard_arrow_down,
                  color: Color(0xFF97080E),
                ),
                value: _selectedcategory,
                items: _dropDownMenuItem,
                hint: Text(
                  'Select Option',
                  style: TextStyle(
                      fontWeight: FontWeight.w600, fontSize: 20, height: 1.1),
                ),
                onChanged: ((String val) {
                  setState(() {
                    _selectedcategory = val;
                  });
                }),
              ),
              SizedBox(height: 20),
              Padding(
                padding: EdgeInsets.only(left: 12.0),
                child: Text(
                  'Rent or Buy',
                  style: _bigFont,
                ),
              ),
              DropdownButtonFormField(
                decoration: InputDecoration(
                  enabled: true,
                  enabledBorder: OutlineInputBorder(
                      borderSide:
                          BorderSide(color: Color(0xFF97080E), width: 4),
                      borderRadius: BorderRadius.all(Radius.circular(12))),
                  focusedBorder: OutlineInputBorder(
                      borderSide:
                          BorderSide(color: Color(0xFF97080E), width: 4),
                      borderRadius: BorderRadius.all(Radius.circular(12))),
                  contentPadding: EdgeInsets.fromLTRB(36, 24, 24, 24),
                ),
                icon: Icon(
                  Icons.keyboard_arrow_down,
                  color: Color(0xFF97080E),
                ),
                value: _selectedtype,
                items: _dropDownMenuItemType,
                hint: Text(
                  'Select Option',
                  style: TextStyle(
                      fontWeight: FontWeight.w600, fontSize: 20, height: 1.1),
                ),
                onChanged: ((String val) {
                  setState(() {
                    _selectedtype = val;
                  });
                }),
              ),
              SizedBox(height: 20),
              Padding(
                padding: EdgeInsets.only(left: 12.0),
                child: Text(
                  'Location',
                  style: _bigFont,
                ),
              ),
              DropdownButtonFormField(
                decoration: InputDecoration(
                  enabled: true,
                  enabledBorder: OutlineInputBorder(
                      borderSide:
                          BorderSide(color: Color(0xFF97080E), width: 4),
                      borderRadius: BorderRadius.all(Radius.circular(12))),
                  focusedBorder: OutlineInputBorder(
                      borderSide:
                          BorderSide(color: Color(0xFF97080E), width: 4),
                      borderRadius: BorderRadius.all(Radius.circular(12))),
                  contentPadding: EdgeInsets.fromLTRB(36, 24, 24, 24),
                ),
                icon: Icon(
                  Icons.keyboard_arrow_down,
                  color: Color(0xFF97080E),
                ),
                value: _selectedlocation,
                items: _dropDownMenuItemLocation,
                hint: Text(
                  'Select Option',
                  style: TextStyle(
                      fontWeight: FontWeight.w600, fontSize: 20, height: 1.1),
                ),
                onChanged: ((String val) {
                  setState(() {
                    _selectedlocation = val;
                  });
                }),
              ),
              SizedBox(height: 20),
              CustomTextField(
                  hinttext: 'Address',
                  isNumber: false,
                  borderColor: Color(0xFF97080E),
                  isPassword: false),
              Padding(
                padding: EdgeInsets.only(left: 12.0),
                child: Text(
                  'SELECT DISTANCE [KM]',
                  style: _bigFont,
                ),
              ),
              SfSliderTheme(
                data: SfSliderThemeData(
                    activeTrackColor: Color(0xFF97080E),
                    inactiveTrackColor: Color(0xFFF7A74D),
                    activeDivisorRadius: 5,
                    inactiveDivisorRadius: 5,
                    activeDivisorColor: Color(0xFF97080E),
                    inactiveDivisorColor: Color(0xFFF7A74D),
                    thumbColor: Color(0xFF97080E)),
                child: SfSlider(
                  min: 0.0,
                  max: 250.0,
                  value: _value,
                  interval: 50,
                  stepSize: 50,
                  showLabels: true,
                  showDivisors: true,
                  onChanged: (dynamic value) {
                    setState(() {
                      _value = value;
                    });
                  },
                ),
              ),
            ],
          ),
        ),
      ),
      bottomNavigationBar: Padding(
        padding: const EdgeInsets.only(left: 100, right: 100, bottom: 10),
        child: MyButton(
            text: 'Search Now',
            color: Color(0xFF97080E),
            onPressed: () {
              Navigator.pushNamed(context, '/search_results');
            }),
      ),
    );
  }
}
