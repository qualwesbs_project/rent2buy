import 'package:flutter/material.dart';
import 'package:rent2buy/widgets/shared_widgets.dart';
import 'package:rent2buy/data.dart';
class Dispute extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: BaseAppBar(
        title: 'Dispute',
        titleColor: Color(0xFF97080E),
        iconTheme: Color(0xFF97080E),
        automaticallyImplyLeading: true,
        widgets: [],
        appBar: AppBar(),
      ),
      body: ListView.builder(
          itemCount: disputeList.length,
          itemBuilder: (BuildContext context, int index) {
            return Column(
              children: [
                Padding(
                  padding: const EdgeInsets.all(12.0),
                  child: ListTile(
                    leading: Image.network(disputeList[index].imageUrl,fit: BoxFit.fitHeight,width: 100,height: 100,),
                    title: Text(
                      disputeList[index].name,
                      style: TextStyle(fontSize: 24),
                    ),
                    subtitle: Padding(
                      padding: const EdgeInsets.only(top:8.0),
                      child: Text(
                        disputeList[index].date,
                        style: TextStyle(fontSize: 16),
                      ),
                    ),
                    trailing: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          '\u0024' + disputeList[index].price.toString(),
                          style:
                              TextStyle(color: Color(0xFF97080E), fontSize: 24),
                        ),
                        Text(
                          disputeList[index].status,
                          style: TextStyle(color: Colors.green[600],fontSize: 16),
                        )
                      ],
                    ),
                    onTap: (){},
                  ),
                ),
                Divider(
                  color: Color(0xFFF7A74D),
                  height: 5.0,
                  thickness: 3,
                ),
              ],
            );
          }),
    );
  }
}


