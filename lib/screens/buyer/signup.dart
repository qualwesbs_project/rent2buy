import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:rent2buy/widgets/shared_widgets.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';

class SignUp extends StatefulWidget {
  @override
  _SignUpState createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  String _email;
  String _password;
  String _name;
  String _number;

  final _key = GlobalKey<FormState>();
  bool _value = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          height: MediaQuery.of(context).size.height,
          child: Column(
            children: [
              Container(
                height: 130.0,
                color: Color(0xFFF7A74D),
                child: Center(
                  child: Text(
                    "R2B",
                    style: TextStyle(
                        fontFamily: 'Rammetto One',
                        color: Color(0xFF97080E),
                        fontSize: 24.0),
                  ),
                ),
              ),
              SizedBox(height: 20),
              Text(
                "Register With Us!",
                style: TextStyle(fontSize: 40, fontWeight: FontWeight.w800),
              ),
              SizedBox(height: 20),
              Form(
                key: _key,
                child: Padding(
                  padding: EdgeInsets.only(left: 20, right: 20),
                  child: Column(
                    children: [
                      AuthTextField(
                        hinttext: 'Full Name',
                        isNumber: false,
                        prefixIcon: Icon(Icons.person_outline),
                        isPassword: false,
                        color: Color(0xFFF7A74D),
                        validator: (String value) {
                          if (value.isEmpty) {
                            return 'Please enter Full Name';
                          }
                          return null;
                        },
                        onSaved: (String value) {
                          _name = value;
                        },
                      ),
                      SizedBox(height: 10),
                      AuthTextField(
                        hinttext: 'Email Address',
                        isNumber: false,
                        prefixIcon: Icon(Icons.email_outlined),
                        isPassword: false,
                        color: Color(0xFFF7A74D),
                        validator: (String value) {
                          Pattern pattern =  r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
                          RegExp regexp = RegExp(pattern);
                          if (value.isEmpty) {
                            return 'Please enter Email';
                          }else if(!regexp.hasMatch(value)){
                            return 'Please enter valid Email';
                          }
                          return null;
                        },
                        onSaved: (String value) {
                          _email = value;
                        },
                      ),
                      SizedBox(height: 10),
                      AuthTextField(
                        hinttext: 'Phone Number',
                        prefixIcon: Icon(Icons.smartphone),
                        isPassword: false,
                        color: Color(0xFFF7A74D),
                        isNumber: true,
                        validator: (String value) {
                          if (value.isEmpty) {
                            return 'Please enter Phone Number';
                          } else if (value.length < 9) {
                            return 'Phone number should be 10 digit';
                          }
                          return null;
                        },
                        onSaved: (String value) {
                          _number = value;
                        },
                      ),
                      SizedBox(height: 10),
                      AuthTextField(
                        hinttext: 'Password',
                        isNumber: false,
                        prefixIcon: Icon(Icons.lock_outline),
                        color: Color(0xFFF7A74D),
                        isPassword: true,
                        validator: (String value) {
                          if (value.isEmpty) {
                            return 'Please enter Password';
                          } else if (value.length < 7) {
                            return 'Password should be greater than 8 digit';
                          }
                          return null;
                        },
                        onSaved: (String value) {
                          _password = value;
                        },
                      ),
                      SizedBox(height: 10),
                    ],
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(left: 20),
                child: Row(
                  children: [
                    Checkbox(
                      value: _value,
                      onChanged: (bool newValue) {
                        setState(() {
                          _value = newValue;
                        });
                      },
                    ),
                    SizedBox(width: 15),
                    Text('Agree with our Terms and Conditions.',
                        style: TextStyle(fontSize: 14, color: Colors.black87)),
                  ],
                ),
              ),
              SizedBox(height: 20),
              MyButton(
                  text: "Register",
                  color: Color(0xFF97080E),
                  onPressed: () {
                    if (_key.currentState.validate()) {
                      _key.currentState.save();
                      FirebaseAuth.instance
                          .createUserWithEmailAndPassword(
                              email: _email, password: _password)
                          .then(
                              (value) => Navigator.of(context).pushNamedAndRemoveUntil('/home', (Route<dynamic> route) => false));
                    }
                  }),
              SizedBox(height: 20),
              Text('OR', style: TextStyle(fontWeight: FontWeight.bold)),
              SizedBox(height: 20),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  RawMaterialButton(
                      child: FaIcon(FontAwesomeIcons.facebookF,
                          size: 24, color: Colors.white),
                      fillColor: Colors.blue[900],
                      shape: CircleBorder(),
                      onPressed: () {}),
                  SizedBox(width: 20),
                  RawMaterialButton(
                      child: FaIcon(FontAwesomeIcons.googlePlusG,
                          size: 24, color: Colors.white),
                      fillColor: Colors.red,
                      shape: CircleBorder(),
                      onPressed: () {}),
                ],
              ),
              Spacer(),
              Row(
                children: [
                  Expanded(
                    child: Divider(
                      color: Color(0xFFF7A74D),
                      height: 5.0,
                      thickness: 5,
                    ),
                  ),
                ],
              ),
              SizedBox(height: 10),
              Align(
                alignment: Alignment.bottomCenter,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(bottom: 20.0),
                      child: RichText(
                        text: TextSpan(
                          children: <TextSpan>[
                            TextSpan(
                                text: 'Already Have an Account? ',
                                style: TextStyle(
                                    fontFamily: 'Dosis',
                                    fontSize: 18,
                                    color: Color(0xFF97080E),
                                    fontWeight: FontWeight.bold)),
                            TextSpan(
                                text: 'Login Here',
                                style: TextStyle(
                                    fontFamily: 'Dosis',
                                    color: Color(0xFFF7A74D),
                                    fontSize: 18,
                                    fontWeight: FontWeight.bold),
                                recognizer: TapGestureRecognizer()
                                  ..onTap = () {
                                    Navigator.pushNamed(context, '/login');
                                  }),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
