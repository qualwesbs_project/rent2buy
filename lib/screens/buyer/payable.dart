import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:rent2buy/widgets/shared_widgets.dart';

class Payable extends StatelessWidget {
  final _textStyle = const TextStyle(fontWeight: FontWeight.w600, fontSize: 26);
  final _textStyleRed = const TextStyle(
      fontWeight: FontWeight.w600, fontSize: 26, color: Color(0xFF97080E));

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: BaseAppBar(
        isCentered: true,
        title: 'Payable',
        titleColor: Color(0xFF97080E),
        iconTheme: Color(0xFF97080E),
        widgets: [],
        automaticallyImplyLeading: true,
        appBar: AppBar(),
      ),
      body: Container(
        padding: EdgeInsets.all(30),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text('Amount', style: _textStyle),
                Text('\$667.30', style: _textStyleRed)
              ],
            ),
            SizedBox(height: 10),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text('By', style: _textStyle),
                Text('William Brouche', style: _textStyle)
              ],
            ),
            SizedBox(height: 10),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text('Payable To', style: _textStyle),
                Text('WT Traders', style: _textStyle)
              ],
            ),
            SizedBox(height: 10),
            Text('Transaction id:', style: _textStyle),
            Text('hdudu-4426-nehyb7-nuebeu',style: TextStyle(fontSize: 18),),
            SizedBox(height: 10),
            Text('Mode:', style: _textStyle),
            Container(
              padding: EdgeInsets.all(16),
              child: Row(
                children: [
                  FaIcon(
                    FontAwesomeIcons.ccAmex,
                    color: Colors.lightBlue,
                    size: 36,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left:8.0),
                    child: Text('American Express',style: _textStyle,),
                  )
                ],
              ),
            ),
            Spacer(),
            Center(child: MyButton(text: 'Select',color: Color(0xFF97080E),onPressed: (){
              Navigator.pushNamed(context, '/confirmed_screen');
            },)),
            Spacer()
          ],
        ),
      ),
    );
  }
}
