import 'package:flutter/material.dart';
import 'package:rent2buy/widgets/shared_widgets.dart';

class Notifications extends StatefulWidget {
  @override
  _NotificationsState createState() => _NotificationsState();
}

class _NotificationsState extends State<Notifications> {
  final List<String> _notifications = ['Order Accepted', 'Order Placed', 'Account Verified', 'Account Created'];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: BaseAppBar(
        title: 'Notifications',
        automaticallyImplyLeading: true,
        titleColor: Color(0xFF97080E),
        iconTheme: Color(0xFF97080E),
        widgets: [],
        appBar: AppBar(),
      ),
      body: Container(
        child: ListView.builder(
            itemCount: _notifications.length,
            itemBuilder: (BuildContext context, int index) {
              return Column(
                children: [
                  ListTile(
                    contentPadding: EdgeInsets.symmetric(horizontal: 50),
                    title: Text(
                      _notifications[index],
                      style: TextStyle(fontWeight: FontWeight.w600, fontSize: 20),
                    ),
                  ),
                  Divider(
                    color: Color(0xFFF7A74D),
                    height: 5.0,
                    thickness: 3,
                  ),
                ],
              );
            }),
      ),
    );
  }
}
