import 'package:carousel_pro/carousel_pro.dart';
import 'package:flutter/material.dart';
import 'package:rent2buy/widgets/shared_widgets.dart';
import 'package:rent2buy/data.dart';

class ProductDetails extends StatefulWidget {
  @override
  _ProductDetailsState createState() => _ProductDetailsState();
}

class _ProductDetailsState extends State<ProductDetails> {
  int index = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: BaseAppBar(
        title: '',
        titleColor: Color(0xFF97080E),
        iconTheme: Color(0xFF97080E),
        automaticallyImplyLeading: true,
        widgets: [
          IconButton(icon: Icon(Icons.shopping_cart), onPressed: () {
            Navigator.pushNamed(context, '/my_cart');
          })
        ],
        appBar: AppBar(),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          GestureDetector(
            onTap: () {
              Navigator.pushNamed(context, '/image_slider');
            },
            child: SizedBox(
              width: MediaQuery.of(context).size.width,
              height: 250,
              child: Carousel(
                images: [
                  NetworkImage(
                    "https://images-na.ssl-images-amazon.com/images/I/71h6PpGaz9L._AC_SL1500_.jpg",
                  ),
                  NetworkImage(
                    "https://images-na.ssl-images-amazon.com/images/I/71h6PpGaz9L._AC_SL1500_.jpg",
                  ),
                ],
                dotSize: 4.0,
                dotBgColor: Colors.transparent,
                dotSpacing: 12.0,
                dotColor: Colors.transparent,
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.all(16),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text('\u0024' + disputeList[index].price.toString(),
                    style: TextStyle(color: Color(0xFF97080E), fontSize: 24)),
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Text(disputeList[index].sellType,
                        style:
                            TextStyle(color: Color(0xFF97080E), fontSize: 24)),
                    Text(disputeList[index].avaliability,
                        style: TextStyle(color: Colors.green, fontSize: 22)),
                  ],
                )
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.only(left: 12, right: 12),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  disputeList[index].name,
                  style: TextStyle(fontSize: 20),
                ),
                SizedBox(height: 10),
                Text(
                  disputeList[index].description,
                  style: TextStyle(fontSize: 20),
                ),
              ],
            ),
          ),
        ],
      ),
      bottomNavigationBar: Padding(
        padding: const EdgeInsets.only(bottom: 12.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            FlatButton(
                onPressed: () {
                  Navigator.pushNamed(context, '/my_cart');
                },
                child: Text(
                  'Add To Cart',
                  style: TextStyle(
                      color: Color(0xFFF7A74D),
                      fontSize: 18,
                      fontWeight: FontWeight.bold),
                )),
            MyButton(
                text: 'Buy Now!',
                color: Color(0xFF97080E),
                onPressed: () {
                  Navigator.pushNamed(context, '/qr_code');
                }),
          ],
        ),
      ),
    );
  }
}
