import 'package:flutter/material.dart';
import 'package:rent2buy/widgets/shared_widgets.dart';
import 'package:syncfusion_flutter_sliders/sliders.dart';
import 'package:syncfusion_flutter_core/theme.dart';

class SearchForRent extends StatefulWidget {
  @override
  _SearchForRentState createState() => _SearchForRentState();
}

class _SearchForRentState extends State<SearchForRent> {
  String _selectedduration;
  String _selectedlocation;
  dynamic _value = 0.0;
  static const _duration = <String>[
    '15 Days',
    '30 Days',
    '60 Days',
  ];
  static const _location = <String>[
    'India',
    'USA',
    'Australia',
  ];

  final List<DropdownMenuItem<String>> _dropDownMenuItems = _duration
      .map(
        (String value) => DropdownMenuItem<String>(
          value: value,
          child: Text(value,
              style: TextStyle(fontWeight: FontWeight.w600, fontSize: 20)),
        ),
      )
      .toList();

  final List<DropdownMenuItem<String>> _dropDownMenuItem = _location
      .map(
        (String value) => DropdownMenuItem<String>(
          value: value,
          child: Text(value,
              style: TextStyle(fontWeight: FontWeight.w600, fontSize: 20)),
        ),
      )
      .toList();

  final _bigFont = const TextStyle(
      fontWeight: FontWeight.w700, fontSize: 24, color: Color(0xFF97080E));

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: BaseAppBar(
        isCentered: true,
        title: 'Search For Rent',
        titleColor: Color(0xFF97080E),
        iconTheme: Color(0xFF97080E),
        automaticallyImplyLeading: true,
        widgets: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Icon(Icons.refresh),
          )
        ],
        appBar: AppBar(),
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 30),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: 30),
              Padding(
                padding: EdgeInsets.only(left: 12.0),
                child: Text(
                  'Duration',
                  style: _bigFont,
                ),
              ),
              DropdownButtonFormField(
                decoration: InputDecoration(
                  enabled: true,
                  enabledBorder: OutlineInputBorder(
                      borderSide:
                          BorderSide(color: Color(0xFF97080E), width: 4),
                      borderRadius: BorderRadius.all(Radius.circular(12))),
                  focusedBorder: OutlineInputBorder(
                      borderSide:
                          BorderSide(color: Color(0xFF97080E), width: 4),
                      borderRadius: BorderRadius.all(Radius.circular(12))),
                  contentPadding: EdgeInsets.fromLTRB(36, 24, 24, 24),
                ),
                icon: Icon(
                  Icons.keyboard_arrow_down,
                  color: Color(0xFF97080E),
                ),
                value: _selectedduration,
                items: _dropDownMenuItems,
                hint: Text(
                  'Select Option',
                  style: TextStyle(
                      fontWeight: FontWeight.w600, fontSize: 20, height: 1.1),
                ),
                onChanged: ((String val) {
                  setState(() {
                    _selectedduration = val;
                  });
                }),
              ),
              SizedBox(height: 20),
              Padding(
                padding: EdgeInsets.only(left: 12.0),
                child: Text(
                  'Location',
                  style: _bigFont,
                ),
              ),
              DropdownButtonFormField(
                decoration: InputDecoration(
                  enabled: true,
                  enabledBorder: OutlineInputBorder(
                      borderSide:
                          BorderSide(color: Color(0xFF97080E), width: 4),
                      borderRadius: BorderRadius.all(Radius.circular(12))),
                  focusedBorder: OutlineInputBorder(
                      borderSide:
                          BorderSide(color: Color(0xFF97080E), width: 4),
                      borderRadius: BorderRadius.all(Radius.circular(12))),
                  contentPadding: EdgeInsets.fromLTRB(36, 24, 24, 24),
                ),
                icon: Icon(
                  Icons.keyboard_arrow_down,
                  color: Color(0xFF97080E),
                ),
                value: _selectedlocation,
                items: _dropDownMenuItem,
                hint: Text(
                  'Select Option',
                  style: TextStyle(
                      fontWeight: FontWeight.w600, fontSize: 20, height: 1.1),
                ),
                onChanged: ((String val) {
                  setState(() {
                    _selectedlocation = val;
                  });
                }),
              ),
              SizedBox(height: 40),
              CustomTextField(
                  hinttext: 'Address',
                  isNumber: false,
                  borderColor: Color(0xFF97080E),
                  isPassword: false),
              SizedBox(height: 30),
              Padding(
                padding: EdgeInsets.only(left: 12.0),
                child: Text(
                  'SELECT DISTANCE [KM]',
                  style: _bigFont,
                ),
              ),
              SfSliderTheme(
                data: SfSliderThemeData(
                    activeTrackColor: Color(0xFF97080E),
                    inactiveTrackColor: Color(0xFFF7A74D),
                    activeDivisorRadius: 5,
                    inactiveDivisorRadius: 5,
                    activeDivisorColor: Color(0xFF97080E),
                    inactiveDivisorColor: Color(0xFFF7A74D),
                    thumbColor: Color(0xFF97080E)),
                child: SfSlider(
                  min: 0.0,
                  max: 250.0,
                  value: _value,
                  interval: 50.0,
                  stepSize: 50.0,
                  showLabels: true,
                  showDivisors: true,
                  onChanged: (dynamic value) {
                    setState(() {
                      _value = value;
                    });
                  },
                ),
              ),
            ],
          ),
        ),
      ),
      bottomNavigationBar: Padding(
        padding: const EdgeInsets.only(left: 140, right: 140, bottom: 10),
        child: MyButton(
            text: 'View',
            color: Color(0xFF97080E),
            onPressed: () {
              Navigator.pushNamed(context, '/rent_confirmation');
            }),
      ),
    );
  }
}
