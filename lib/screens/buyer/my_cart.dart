import 'package:flutter/material.dart';
import 'package:rent2buy/widgets/shared_widgets.dart';

class MyCart extends StatefulWidget {
  @override
  _MyCartState createState() => _MyCartState();
}

class _MyCartState extends State<MyCart> {
  String qty = 'Qty 1';
  final _textStyle = const TextStyle(
      fontWeight: FontWeight.w600, fontSize: 16, color: Color(0xff717171));

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: BaseAppBar(
        title: 'My Cart',
        automaticallyImplyLeading: true,
        titleColor: Color(0xFF97080E),
        iconTheme: Color(0xFF97080E),
        widgets: [IconButton(icon: Icon(Icons.delete), onPressed: () {})],
        appBar: AppBar(),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            padding: EdgeInsets.symmetric(horizontal: 36),
            height: 60,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  'Select Address',
                  style: TextStyle(
                      color: Color(0xFF97080E),
                      fontWeight: FontWeight.w600,
                      fontSize: 24),
                ),
                Icon(
                  Icons.arrow_forward_ios_sharp,
                  color: Color(0xFF97080E),
                )
              ],
            ),
          ),
          Divider(
            color: Color(0xFFF7A74D),
            height: 5.0,
            thickness: 3,
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 12),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'Samsung 65 Inch',
                          style: TextStyle(
                              fontWeight: FontWeight.w600, fontSize: 24),
                          overflow: TextOverflow.ellipsis,
                        ),
                        Text(
                          'Size:65-Inch, Black',
                          style: _textStyle,
                        ),
                        Text(
                          'Seller: Samsung WT Traders',
                          style: _textStyle,
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Text(
                          '\$663.30',
                          style: TextStyle(fontSize: 28),
                        ),
                        Text(
                          'Delivery by Tue 21 Jul',
                          style: _textStyle,
                        )
                      ],
                    ),
                    Column(
                      children: [
                        Image.network(
                          'https://images-na.ssl-images-amazon.com/images/I/71h6PpGaz9L._AC_SL1500_.jpg',
                          width: 120,
                          height: 120,
                        ),
                        DropdownButtonHideUnderline(
                            child: Container(
                          width: 80,
                          padding: EdgeInsets.only(left: 10, right: 10),
                          decoration: ShapeDecoration(
                              shape: OutlineInputBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(12)))),
                          child: DropdownButton(
                              value: qty,
                              items: <String>['Qty 1', 'Qty 2']
                                  .map((String value) {
                                return DropdownMenuItem<String>(
                                    value: value, child: Text(value));
                              }).toList(),
                              onChanged: (String newVal) {
                                setState(() {
                                  qty = newVal;
                                });
                              }),
                        ))
                      ],
                    )
                  ],
                ),
              ],
            ),
          ),
          SizedBox(height: 30),
          FlatButton.icon(
            icon: Icon(Icons.delete),
            label: Text(
              'Remove Item',
              style: TextStyle(fontSize: 18),
            ),
            onPressed: () {},
          ),
          Container(
            padding: EdgeInsets.all(18),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'PRICE DETAILS',
                  style: TextStyle(fontSize: 18, color: Colors.grey),
                ),
                SizedBox(
                  height: 10,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text('Price (1 Item)', style: TextStyle(fontSize: 20)),
                    Text(
                      '\$663.30',
                      style: TextStyle(fontSize: 20, color: Colors.grey),
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text('Delivery Fee', style: TextStyle(fontSize: 20)),
                    Text(
                      '\$2.00',
                      style: TextStyle(fontSize: 20, color: Colors.grey),
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text('Taxes', style: TextStyle(fontSize: 20)),
                    Text(
                      '\$2.00',
                      style: TextStyle(fontSize: 20, color: Colors.grey),
                    ),
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text('Total Amount', style: TextStyle(fontSize: 20)),
                    Text(
                      '\$667.30',
                      style: TextStyle(fontSize: 20),
                    ),
                  ],
                )
              ],
            ),
          ),
          Spacer(),
          FlatButton(
              onPressed: () {
                Navigator.pushNamed(context, '/chats');
              },
              child: Text(
                'Chat with Dealer',
                style: TextStyle(color: Color(0xFF97080E), fontSize: 18),
              )),
          Spacer()
        ],
      ),
      bottomNavigationBar: Container(
        height: 80,
        child: Column(
          children: [
            Divider(
              color: Color(0xFFF7A74D),
              height: 5.0,
              thickness: 3,
            ),
            Padding(
              padding:
                  const EdgeInsets.symmetric(vertical: 8.0, horizontal: 20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Text(
                    '\$663.30',
                    style: TextStyle(fontSize: 32, color: Color(0xFF97080E)),
                  ),
                  MyButton(
                    text: 'Place Order',
                    color: Color(0xFF97080E),
                    onPressed: () {
                      Navigator.pushNamed(context, '/confirmed_screen');
                    },
                  )
                ],
              ),
            ),
            Divider(
              color: Color(0xFFF7A74D),
              height: 5.0,
              thickness: 3,
            ),
          ],
        ),
      ),
    );
  }
}
