import 'package:flutter/material.dart';
import 'package:rent2buy/widgets/shared_widgets.dart';

class Profile extends StatefulWidget {
  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xFFF7A74D),
        title: Text('R2B',
            style: TextStyle(
                fontFamily: 'Rammetto One',
                color: Color(0xFF97080E),
                fontSize: 24)),
        centerTitle: true,
        automaticallyImplyLeading: false,
      ),
      body: Container(
        padding: EdgeInsets.all(20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            SizedBox(height: 20),
            Text(
              'Profile',
              style: TextStyle(fontWeight: FontWeight.w700, fontSize: 28),
            ),
            SizedBox(height: 30),
            AuthTextField(
                hinttext: 'Full Name',
                prefixIcon: Icon(Icons.person_outline),
                isPassword: false,
                color: Color(0xFFF7A74D),
                isNumber: false,
                suffixIcon: Icon(Icons.edit)),
            SizedBox(height: 10),
            AuthTextField(
                hinttext: 'Email Address',
                prefixIcon: Icon(Icons.email_outlined),
                isPassword: false,
                color: Color(0xFFF7A74D),
                isNumber: false,
                suffixIcon: Icon(Icons.edit)),
            SizedBox(height: 10),
            AuthTextField(
                hinttext: 'Phone Number',
                prefixIcon: Icon(Icons.smartphone),
                isPassword: false,
                isNumber: true,
                color: Color(0xFFF7A74D),
                suffixIcon: Icon(Icons.edit),
            ),
            SizedBox(height: 10),
            AuthTextField(
              hinttext: 'Password',
              prefixIcon: Icon(Icons.lock_outline),
              isPassword: true,
              color: Color(0xFFF7A74D),
              isNumber: false,
            ),
            SizedBox(height: 10),
            Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    FlatButton(
                        onPressed: () {
                          Navigator.pushNamed(context, "/forget_pwd");
                        },
                        materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                        child: Text(
                          'Forget Password?',
                          style: TextStyle(color: Colors.grey[500]),
                        )),
                  ],
                ),
                Row(mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    FlatButton(
                        onPressed: () {
                          Navigator.pushNamed(context, "/reset_pwd");
                        },
                        materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                        child: Text(
                          'Reset Password?',
                          style: TextStyle(color: Colors.grey[500]),
                        )),
                  ],
                ),
              ],
            ),
            Spacer(),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: MyButton(text: 'Save Changes',color: Color(0xFF97080E), onPressed: () {}),
            ),
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: Text(
                'Cancel',
                style: TextStyle(fontSize: 16, color: Color(0xFFF7A74D)),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
