import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:rent2buy/models/user_model.dart';
import 'package:rent2buy/widgets/received_msg_widget.dart';
import 'package:rent2buy/widgets/sended_msg_widget.dart';
import 'package:rent2buy/widgets/shared_widgets.dart';

class ChatScreen extends StatefulWidget {
  @override
  _ChatScreenState createState() => _ChatScreenState();
}

class _ChatScreenState extends State<ChatScreen> {
  User user;
  TextEditingController _text = TextEditingController();
  ScrollController _scrollController = ScrollController();
  var childList = <Widget>[];

  @override
  void initState() {
    super.initState();
    childList.add(Align(
      alignment: Alignment(0, 0),
      child: Container(
        margin: EdgeInsets.only(top: 5.0),
        height: 25,
        width: 50,
        decoration: BoxDecoration(
            color: Colors.black12,
            borderRadius: BorderRadius.all(Radius.circular(8.0))),
        child: Center(
          child: Text('Today', style: TextStyle(fontSize: 11)),
        ),
      ),
    ));
    childList.add(Align(
      alignment: Alignment(1, 0),
      child: SendedMsgWidget(content: 'Hello'),
    ));
    childList.add(Align(
      alignment: Alignment(1, 0),
      child: SendedMsgWidget(content: 'How Are You'),
    ));
    childList.add(Align(
      alignment: Alignment(-1, 0),
      child: ReceivedMsgWidget(content: 'How Are You'),
    ));
    childList.add(Align(
      alignment: Alignment(-1, 0),
      child: ReceivedMsgWidget(content: 'How Are You'),
    ));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: BaseAppBar(
          title: 'User 1',
          titleColor: Color(0xFF97080E),
          iconTheme: Color(0xFF97080E),
          automaticallyImplyLeading: true,
          widgets: [
            PopupMenuButton(itemBuilder: (BuildContext context) {
              return [
                PopupMenuItem(child: Text('Pay Now')),
                PopupMenuItem(child: Text('Details')),
              ];
            })
          ],
          appBar: AppBar(),
        ),
        body: SafeArea(
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 6),
          child: Stack(
            fit: StackFit.loose,
            children: [
              Column(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SingleChildScrollView(
                    controller: _scrollController,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: childList,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(14, 12, 14, 12),
                    child: TextField(
                      controller: _text,
                      decoration: InputDecoration(
                        prefixIcon: Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Image.asset('assets/speech-bubbles.png',height: 10,color: Colors.grey,),
                        ) ,
                        suffixIcon: IconButton(icon: Icon(Icons.send),onPressed: (){},),
                        enabled: true,
                        enabledBorder: OutlineInputBorder(
                            borderSide:
                                BorderSide(color: Color(0xFF97080E), width: 3),
                            borderRadius:
                                BorderRadius.all(Radius.circular(12))),
                        focusedBorder: OutlineInputBorder(
                            borderSide:
                                BorderSide(color: Color(0xFF97080E), width: 4),
                            borderRadius:
                                BorderRadius.all(Radius.circular(12))),
                        hintText: 'Message',
                      ),
                    ),
                  )
                ],
              )
            ],
          ),
        )));
  }
}
