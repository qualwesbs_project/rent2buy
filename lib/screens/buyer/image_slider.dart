import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:rent2buy/widgets/shared_widgets.dart';
import 'dart:math' as math;

class ImageSlider extends StatelessWidget {
  CarouselController buttonCarouselController = CarouselController();
  final List<String> imgList = [
    "https://images-na.ssl-images-amazon.com/images/I/71h6PpGaz9L._AC_SL1500_.jpg",
    "https://images-na.ssl-images-amazon.com/images/I/71h6PpGaz9L._AC_SL1500_.jpg",
    "https://images-na.ssl-images-amazon.com/images/I/71h6PpGaz9L._AC_SL1500_.jpg",
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: BaseAppBar(
        title: '',
        titleColor: Color(0xFF97080E),
        iconTheme: Color(0xFF97080E),
        automaticallyImplyLeading: true,
        widgets: [],
        appBar: AppBar(),
      ),
      body: Stack(children: [
        Center(
          child: CarouselSlider(
              items: imgList
                  .map((item) => Container(
                        child: Center(
                          child: Image.network(
                            item,
                            fit: BoxFit.cover,
                            width: MediaQuery.of(context).size.width,
                          ),
                        ),
                      ))
                  .toList(),
              carouselController: buttonCarouselController,
              options: CarouselOptions()),
        ),
        Center(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
            Transform(
              alignment: Alignment.center,
              transform: Matrix4.rotationY(math.pi),
              child: IconButton(
                  icon: Icon(Icons.play_arrow),
                  color: Color(0xFFF7A74D),
                  onPressed: () {
                    buttonCarouselController.previousPage(
                        duration: Duration(milliseconds: 300), curve: Curves.linear);
                  })),
            IconButton(icon: Icon(Icons.play_arrow), onPressed: () {
              buttonCarouselController.nextPage(
                  duration: Duration(milliseconds: 300), curve: Curves.linear);
            },
              color: Color(0xFFF7A74D),),
            ],
          ),
        )
      ]),
    );
  }
}
