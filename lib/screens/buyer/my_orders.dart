import 'package:flutter/material.dart';
import 'package:rent2buy/models/product_model.dart';
import 'package:rent2buy/widgets/shared_widgets.dart';

class MyOrders extends StatelessWidget {
  List<ProductModel> myOrderList = [
    ProductModel(
        "Guitar",
        'abchdhej',
        'https://images-na.ssl-images-amazon.com/images/I/71h6PpGaz9L._AC_SL1500_.jpg',
        '12/11/2020',
        30.0,
        'Bought',
        'TV',
        10.0,
        'Buy',
        'Avaliable'
    )
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: BaseAppBar(
        title: 'My Orders',
        automaticallyImplyLeading: true,
        titleColor: Color(0xFF97080E),
        iconTheme: Color(0xFF97080E),
        widgets: [],
        appBar: AppBar(),
      ),
      body: ListView.builder(
          itemCount: myOrderList.length,
          itemBuilder: (BuildContext context, int index) {
            return Column(
              children: [
                Padding(
                  padding: const EdgeInsets.all(12.0),
                  child: ListTile(
                    leading: Image.network(
                      myOrderList[index].imageUrl,
                      fit: BoxFit.fitHeight,
                      width: 100,
                      height: 100,
                    ),
                    title: Text(
                      myOrderList[index].name,
                      style: TextStyle(fontSize: 24),
                    ),
                    subtitle: Padding(
                      padding: const EdgeInsets.only(top: 8.0),
                      child: Text(
                        myOrderList[index].date,
                        style: TextStyle(fontSize: 16),
                      ),
                    ),
                    trailing: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          '\u0024' + myOrderList[index].price.toString(),
                          style:
                              TextStyle(color: Color(0xFF97080E), fontSize: 24),
                        ),
                        Text(
                          myOrderList[index].status,
                          style:
                              TextStyle(color: Colors.green[600], fontSize: 16),
                        )
                      ],
                    ),
                    onTap: () {
                      Navigator.pushNamed(context, "/product_detail");                    },
                  ),
                ),
                Divider(
                  color: Color(0xFFF7A74D),
                  height: 5.0,
                  thickness: 3,
                ),
              ],
            );
          }),
    );
  }
}
