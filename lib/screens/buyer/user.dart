import 'package:flutter/material.dart';
import 'package:rent2buy/widgets/shared_widgets.dart';

class User extends StatefulWidget {
  @override
  _UserState createState() => _UserState();
}

class _UserState extends State<User> {
  final List<String> _user = ['Profile', 'Here to Buy', 'Take On Rent'];
  final List<String> _routes = ['profile','chats','search_for_rent'];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: BaseAppBar(
        isCentered: true,
        title: 'User',
        titleColor: Color(0xFF97080E),
        iconTheme: Color(0xFF97080E),
        automaticallyImplyLeading: true,
        widgets: [],
        appBar: AppBar(),
      ),
      body: Container(
        padding: EdgeInsets.only(top: 20),
        child: ListView.builder(
            itemCount: _user.length,
            itemBuilder: (BuildContext context, int index) {
              return Column(
                children: [
                  ListTile(
                    contentPadding: EdgeInsets.symmetric(horizontal: 50),
                    title: Text(
                      _user[index],
                      style: TextStyle(fontWeight: FontWeight.w600, fontSize: 20),
                    ),
                    trailing: Icon(Icons.arrow_forward_ios,color: Colors.black54,size: 16,),
                    onTap: (){Navigator.pushNamed(context, '/${_routes[index]}');},
                  ),
                  Divider(
                    color: Color(0xFFF7A74D),
                    height: 5.0,
                    thickness: 3,
                  ),
                ],
              );
            }),
      ),
    );
  }
}
