import 'package:flutter/material.dart';
import 'package:rent2buy/widgets/shared_widgets.dart';

class OtpVerify extends StatefulWidget {
  @override
  _OtpVerifyState createState() => _OtpVerifyState();
}

class _OtpVerifyState extends State<OtpVerify> {
  TextEditingController _textEditingController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: appBar(Color(0xFF97080E)),
        body: Container(
          padding: EdgeInsets.symmetric(horizontal: 40),
          child: Column(
            children: [
              SizedBox(height: 60),
              CustomTextField(
                  hinttext: 'Enter OTP',
                  isPassword: false,
                  borderColor: Color(0xFF97080E),
                  isNumber: true,
                  textEditingController: _textEditingController),
              SizedBox(height: 40),
              Center(
                  child: MyButton(
                      text: 'Verify',
                      color: Color(0xFF97080E),
                      onPressed: () {
                        Navigator.pushNamed(context, '/reset_pwd');
                      }))
            ],
          ),
        ),
      ),
    );
  }
}
