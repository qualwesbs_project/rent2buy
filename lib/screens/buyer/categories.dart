import 'package:flutter/material.dart';
import 'package:rent2buy/screens/buyer/sub_categories.dart';
import 'package:rent2buy/widgets/shared_widgets.dart';
import 'package:rent2buy/models/category_model.dart';
class Categories extends StatelessWidget {

  List<CategoryModel> _categories = [
    CategoryModel('Electronics', [
      'Laptops',
      'Mobiles',
      'Tablets',
      'Computer',
      'Camera',
      'Powerbanks',
      'Smart Watch',
      'Printers & Scanners',
      'Monitors & more'
    ]),
    CategoryModel("Home Decor", []),
    CategoryModel('TV & Appliances', []),
    CategoryModel("Fashion", []),
    CategoryModel('Camera', []),
    CategoryModel("Sports & more", []),
    CategoryModel('Musical Instruments', []),
    CategoryModel("Cars", []),
    CategoryModel('Bikes', []),
    CategoryModel("Bicycle", []),
    CategoryModel('House', []),
    CategoryModel("Apartment", []),
    CategoryModel('Clothing', []),
    CategoryModel("Costumes", []),
    CategoryModel('Accessories', []),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: BaseAppBar(
        title: 'All categories',
        automaticallyImplyLeading: true,
        titleColor: Color(0xFF97080E),
        iconTheme: Color(0xFF97080E),
        widgets: [],
        appBar: AppBar(),
      ),
      body: ListView.builder(
          itemCount: _categories.length,
          itemBuilder: (BuildContext context, int index) {
            return Column(
              children: [
                ListTile(
                  contentPadding: EdgeInsets.symmetric(horizontal: 50),
                  title: Text(
                    _categories[index].category,
                    style: TextStyle(fontWeight: FontWeight.w600, fontSize: 20),
                  ),
                  //onTap: (){Navigator.pushNamed(context, '/${_routes[index]}');},
                  onTap: () {
                    Navigator.push(context, MaterialPageRoute(
                        builder: (context) =>
                            SubCategory(categoryModel: _categories[index])));
                  },
                ),
                Divider(
                  color: Color(0xFFF7A74D),
                  height: 5.0,
                  thickness: 3,
                ),
              ],
            );
          }),
    );
  }
}
