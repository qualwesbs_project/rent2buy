import 'package:flutter/material.dart';
import 'package:rent2buy/theme.dart';
import 'package:rent2buy/widgets/shared_widgets.dart';

class SellerOTPVerify extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Theme(
      data: sellerTheme,
      child: Scaffold(
        appBar: appBar(Color(0xFF003585)),
        body: Container(
          padding: EdgeInsets.symmetric(horizontal: 40),
          child: Column(
            children: [
              SizedBox(height: 60),
              CustomTextField(
                hinttext: 'Enter OTP',
                isPassword: false,
                borderColor: Color(0xFF003585),
                isNumber: true,
              ),
              SizedBox(height: 40),
              Center(
                  child: MyButton(
                      text: 'Verify',
                      color: Color(0xFF003585),
                      onPressed: () {
                        Navigator.pushNamed(context, '/seller_reset_pwd');
                      }))
            ],
          ),
        ),
      ),
    );
  }
}
