import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:rent2buy/theme.dart';
import 'package:rent2buy/widgets/shared_widgets.dart';

class SellerProfile extends StatelessWidget {
  final _bigFont = const TextStyle(
      fontSize: 20, fontWeight: FontWeight.w700);
  final padding = const EdgeInsets.symmetric(horizontal: 50,vertical: 12);

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: sellerTheme,
      child: Scaffold(
        appBar: BaseAppBar(
          appBar: AppBar(),
          title: 'Seller Profile',
          isCentered: true,
          automaticallyImplyLeading: true,
          iconTheme: Color(0xFF003585),
          titleColor: Color(0xFF003585),
        ),
        body: ListView(
          children: [
            ListTile(
              contentPadding: padding,
              leading: Icon(Icons.cloud_upload_outlined,color: Colors.black,size: 32,),
              title: Text('Add Listing',style: _bigFont,),
              onTap: (){
                Navigator.pushNamed(context, '/add_listing');
              },
            ),
            CustomDivider(),
            ListTile(
              contentPadding: padding,
              leading: Icon(Icons.account_balance_outlined,color: Colors.black,size: 32,),
              title: Text('Add Bank Account',style: _bigFont,),
              onTap: (){
                Navigator.pushNamed(context, '/bank_account');
              },
            ),
            CustomDivider(),
            ListTile(
              contentPadding: padding,
              leading: Image.asset('assets/analytics.png',height: 34,),
              title: Text('Report',style: _bigFont,),
              onTap: (){
                Navigator.pushNamed(context, '/selling_report');
              },
            ),
            CustomDivider(),
            ListTile(
              contentPadding: padding,
              leading: Icon(Icons.settings_outlined,color: Colors.black,size: 32,),
              title: Text('Settings',style: _bigFont,),
              onTap: (){
                Navigator.pushNamed(context, '/seller_settings');
              },
            ),
            CustomDivider(),
          ],
        ),
      ),
    );
  }
}


