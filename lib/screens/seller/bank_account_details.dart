import 'package:flutter/material.dart';
import 'package:rent2buy/theme.dart';
import 'package:rent2buy/widgets/shared_widgets.dart';

class BankAccount extends StatefulWidget {
  @override
  _BankAccountState createState() => _BankAccountState();
}

class _BankAccountState extends State<BankAccount> {
  @override
  Widget build(BuildContext context) {
    return Theme(
        data: sellerTheme,
        child: Scaffold(
          appBar: BaseAppBar(
            appBar: AppBar(),
            title: 'Bank Account Details',
            isCentered: false,
            automaticallyImplyLeading: true,
            iconTheme: Color(0xFF003585),
            titleColor: Color(0xFF003585),
          ),
          body: Container(
            padding: EdgeInsets.fromLTRB(20, 30, 20, 20),
            child: Column(
              children: [
                Card(
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(16)),
                  color: Color(0xFF71C7E3),
                  child: Container(
                    height: 100,
                    child: ListTile(
                      contentPadding: EdgeInsets.only(top:12,left: 12,right: 12),
                      leading: Padding(
                        padding: const EdgeInsets.only(left:6.0,top: 8),
                        child: Image.asset('assets/mastercard.png',fit: BoxFit.fill,),
                      ),
                      title: Text('Bank of America',style: TextStyle(color: Colors.black,fontSize: 24,fontWeight: FontWeight.w600),),
                      subtitle: Padding(
                        padding: const EdgeInsets.only(top:16.0),
                        child: Text('XXXXXXXXXXXXXXXXX678'),
                      ),
                      trailing: Icon(Icons.check_circle_outline,size: 36,color: Colors.green.shade700,),
                    ),
                  ),
                ),
                SizedBox(
                  height: 30,
                ),
                Row(
                  children: [
                    Expanded(child: MyButton(text: 'Add Bank Account',onPressed: (){},color: Color(0xFF003585),))
                  ],
                ),
                SizedBox(
                  height: 20,
                ),
                Row(
                  children: [
                    Expanded(child: MyButton(text: 'Remove Account',onPressed: (){},color: Color(0xFF003585),))
                  ],
                )
              ],
            ),
          ),
        )
    );
  }
}
