import 'package:flutter/material.dart';
import 'package:rent2buy/theme.dart';
import 'dart:math' as math;
import 'package:rent2buy/widgets/shared_widgets.dart';

class SellerAppSettings extends StatelessWidget {
  final _bigFont = const TextStyle(
      fontSize: 20, fontWeight: FontWeight.w600);
  @override
  Widget build(BuildContext context) {
    return Theme(
      data: sellerTheme,
      child: Scaffold(
        appBar: BaseAppBar(
          appBar: AppBar(),
          title: 'App Settings',
          titleColor: Color(0xFF003585),
          iconTheme: Color(0xFF003585),
          automaticallyImplyLeading: true,
        ),
        body: Container(
          padding: EdgeInsets.only(top: 20),
          child: ListView(
            children: [
              ListTile(
                contentPadding: EdgeInsets.symmetric(horizontal: 50),
                title: Text(
                  'About',
                  style: _bigFont,
                ),
                subtitle: Text('Rent and sell items at freat prices'),
                onTap: (){},
              ),
              Divider(
                color: Color(0xFF71C7E3),
                height: 5.0,
                thickness: 3,
              ),
              ListTile(
                contentPadding: EdgeInsets.symmetric(horizontal: 50),
                title: Text('Share this',style:_bigFont),
                onTap: (){},
                leading: Transform(alignment: Alignment.center,transform: Matrix4.rotationY(math.pi),child: Icon(Icons.reply,color: Colors.black,)),
              ),
              Divider(
                  color: Color(0xFF71C7E3),
                  height: 5.0,
                  thickness: 3
              ),
              ListTile(
                contentPadding: EdgeInsets.symmetric(horizontal: 50),
                title: Text('Please Rate us',style:_bigFont,),
                onTap: (){},
                leading: Icon(Icons.star,color: Colors.black,),
              ),
              Divider(
                color: Color(0xFF71C7E3),
                height: 5.0,
                thickness: 3,
              ),
              ListTile(
                contentPadding: EdgeInsets.symmetric(horizontal: 50),
                title: Text(
                  'Feedback',
                  style: _bigFont,
                ),
                subtitle: Text('Got any queries? We are here to help you!'),
                onTap: (){},
              ),
              Divider(
                color: Color(0xFF71C7E3),
                height: 5.0,
                thickness: 3,
              ),
              ListTile(
                contentPadding: EdgeInsets.symmetric(horizontal: 50),
                title: Text(
                  'Dispute',
                  style: _bigFont,
                ),
                onTap: (){
                  Navigator.pushNamed(context, '/seller_dispute');
                },
              ),
              Divider(
                color: Color(0xFF71C7E3),
                height: 5.0,
                thickness: 3,
              ),
              ListTile(
                contentPadding: EdgeInsets.symmetric(horizontal: 50),
                title: Text(
                  'Contact Us',
                  style: _bigFont,
                ),
                onTap: (){},
              ),
              Divider(
                color: Color(0xFF71C7E3),
                height: 5.0,
                thickness: 3,
              ),
              ListTile(
                contentPadding: EdgeInsets.symmetric(horizontal: 50),
                title: Text(
                  'Privacy Policy',
                  style: _bigFont,
                ),
                onTap: (){},
              ),
              Divider(
                color: Color(0xFF71C7E3),
                height: 5.0,
                thickness: 3,
              ),
              ListTile(
                contentPadding: EdgeInsets.symmetric(horizontal: 50),
                title: Text(
                  'Terms and Conditions',
                  style: _bigFont,
                ),
                onTap: (){},
              ),
              Divider(
                color: Color(0xFF71C7E3),
                height: 5.0,
                thickness: 3,
              ),

            ],
          ),
        ),
      ),
    );
  }
}
