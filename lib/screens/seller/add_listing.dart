import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:rent2buy/theme.dart';
import 'package:rent2buy/widgets/shared_widgets.dart';

class AddListing extends StatefulWidget {
  @override
  _AddListingState createState() => _AddListingState();
}

class _AddListingState extends State<AddListing> {
  bool _deliveryType;
  bool _checkBoxVal = false;

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: sellerTheme,
      child: Scaffold(
        resizeToAvoidBottomPadding: false,
        appBar: BaseAppBar(
          appBar: AppBar(),
          title: 'Add Listing',
          isCentered: true,
          automaticallyImplyLeading: true,
          iconTheme: Color(0xFF003585),
          titleColor: Color(0xFF003585),
        ),
        body: Container(
          padding: EdgeInsets.symmetric(horizontal: 30, vertical: 10),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              AuthTextField(
                hinttext: 'Item Name',
                color: Color(0xFF71C7E3),
                isNumber: false,
                isPassword: false,
              ),
              AuthTextField(
                hinttext: 'Choose Category',
                color: Color(0xFF71C7E3),
                isNumber: false,
                isPassword: false,
              ),
              Row(
                children: [
                  Expanded(
                    child: ListTile(
                        leading: Radio(
                            value: false,
                            groupValue: _deliveryType,
                            onChanged: (val) {
                              _deliveryType = val;
                              setState(() {

                              });
                            }),
                        title: Text('Self Pickup',style: TextStyle(color: Colors.grey[600]))),
                  ),
                  Expanded(
                    child: ListTile(
                        leading: Radio(
                            value: true,
                            groupValue: _deliveryType,
                            onChanged: (val) {
                              _deliveryType = val;
                              setState(() {

                              });
                            }),
                        title: Text('Delivery',style: TextStyle(color: Colors.grey[600]))),
                  ),
                ],
              ),
              AuthTextField(
                hinttext: 'Phone Number',
                color: Color(0xFF71C7E3),
                isNumber: true,
                isPassword: false,
              ),
              AuthTextField(
                hinttext: 'Price / Your Bid',
                color: Color(0xFF71C7E3),
                isNumber: false,
                isPassword: false,
              ),
              AuthTextField(
                hinttext: 'Description',
                color: Color(0xFF71C7E3),
                isNumber: false,
                isPassword: false,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Row(
                    children: [
                      Checkbox(value: _checkBoxVal, onChanged: (val){
                        setState(() {
                          _checkBoxVal = val;
                        });
                      }),
                      Text('Sell',style: TextStyle(color: Colors.grey[600]))
                    ],
                  ),
                  Row(
                    children: [
                      Checkbox(value: _checkBoxVal, onChanged: (val){
                        setState(() {
                          _checkBoxVal = val;
                        });
                      }),
                      Text('Rent',style: TextStyle(color: Colors.grey[600]),)
                    ],
                  ),
                  Row(
                    children: [
                      Checkbox(value: _checkBoxVal, onChanged: (val){
                        setState(() {
                          _checkBoxVal = val;
                        });
                      }),
                      Text('Both',style: TextStyle(color: Colors.grey[600]))
                    ],
                  )
                ],
              ),
              MyButton(color: Color(0xFF003585),text: 'Upload Image',onPressed: (){},),
              MyButton(color: Color(0xFF003585),text: 'Submit',onPressed: (){
                Navigator.pushNamed(context, '/item_added');
              },)
            ],
          ),
        ),
      ),
    );
  }
}
