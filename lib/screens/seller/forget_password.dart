import 'package:flutter/material.dart';
import 'package:rent2buy/theme.dart';
import 'package:rent2buy/widgets/shared_widgets.dart';

class SellerForgetPassword extends StatelessWidget {
  TextEditingController _textEditingController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: sellerTheme,
      child: Scaffold(
        appBar: appBar(Color(0xFF003585)),
        body: Container(
          height: MediaQuery.of(context).size.height,
          padding: EdgeInsets.symmetric(horizontal: 40),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: 60),
              Padding(
                padding: EdgeInsets.only(left: 12.0),
                child: Text(
                  'Forget Password',
                  style: TextStyle(fontWeight: FontWeight.w700, fontSize: 24),
                ),
              ),
              SizedBox(height: 20),
              CustomTextField(
                hinttext: 'Enter Email',
                borderColor: Color(0xFF003585),
                isNumber: false,
                isPassword: false,
                textEditingController: _textEditingController,
              ),
              SizedBox(height: 20),
              Padding(
                padding: const EdgeInsets.only(left: 12.0),
                child: Text(
                    'A link will be shared to your email to generate new password',
                    style: TextStyle(
                        color: Colors.grey[700],
                        fontWeight: FontWeight.w500,
                        fontSize: 16)),
              ),
              SizedBox(height: 30),
              Center(
                  child: MyButton(
                      text: 'Submit',
                      color: Color(0xFF003585),
                      onPressed: () {
                        Navigator.pushNamed(context, '/seller_OTP_verify');
                      }))
            ],
          ),
        ),
      ),
    );
  }
}
