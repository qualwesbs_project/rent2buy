import 'package:flutter/material.dart';
import 'package:rent2buy/data.dart';
import 'package:rent2buy/models/product_model.dart';
import 'package:rent2buy/widgets/shared_widgets.dart';

class OrderPending extends StatelessWidget {

  List<ProductModel> _orderPending = [
    ProductModel("Laptop", 'Description', 'https://images-na.ssl-images-amazon.com/images/I/71h6PpGaz9L._AC_SL1500_.jpg', '12/11/2020', 30.0, 'Sell', 'TV',10.0 , 'Buy Only','Avaliable'),
  ];
  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListView.builder(
          itemCount: _orderPending.length,
          itemBuilder: (BuildContext context, int index) {
            return Column(
              children: [
                Padding(
                  padding: const EdgeInsets.all(12.0),
                  child: ListTile(
                    leading: Image.network(disputeList[index].imageUrl,fit: BoxFit.fitHeight,width: 100,height: 100,),
                    title: Text(
                      _orderPending[index].name,
                      style: TextStyle(fontSize: 24),
                    ),
                    subtitle: Padding(
                      padding: const EdgeInsets.only(top:8.0),
                      child: Text(
                        _orderPending[index].date,
                        style: TextStyle(fontSize: 16),
                      ),
                    ),
                    trailing: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          '\u0024' + _orderPending[index].price.toString(),
                          style:
                          TextStyle(color: Color(0xFF97080E), fontSize: 24),
                        ),
                        Text(
                          _orderPending[index].status,
                          style: TextStyle(color: Colors.green[600],fontSize: 16),
                        )
                      ],
                    ),
                    onTap: (){
                      Navigator.pushNamed(context, '/seller_product_detail');

                    },
                  ),
                ),
                CustomDivider()
              ],
            );
          }),
    );
  }
}
