import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:rent2buy/widgets/shared_widgets.dart';
import '../../theme.dart';

class SellerLogin extends StatefulWidget {
  @override
  _SellerLoginState createState() => _SellerLoginState();
}

class _SellerLoginState extends State<SellerLogin> {
  final _key = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: sellerTheme,
      child: Scaffold(
        body: SingleChildScrollView(
          child: Container(
            height: MediaQuery.of(context).size.height,
            child: Column(
              children: [
                Container(
                  height: 200.0,
                  color: Color(0xFF71C7E3),
                  child: Center(
                    child: Text(
                      "Rent 2 Buy",
                      style: TextStyle(
                          fontFamily: 'Interval Sans Pro',
                          color: Color(0xFF003585),
                          fontWeight: FontWeight.bold,
                          fontSize: 50.0,
                          shadows: [Shadow(color: Colors.white,blurRadius: 1.0,offset: Offset(-2.0, 2.0))]
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 20),
                Text(
                  "Welcome Back!",
                  style: TextStyle(fontSize: 40, fontWeight: FontWeight.w800),
                ),
                SizedBox(height: 30),
                Form(
                  key: _key,
                  child: Padding(
                    padding: const EdgeInsets.only(left: 20.0, right: 20),
                    child: Column(
                      children: [
                        AuthTextField(
                          hinttext: 'Email Address',
                          prefixIcon: Icon(Icons.email_outlined),
                          isPassword: false,
                          color: Color(0xFF71C7E3),
                          isNumber: false,
                        ),
                        SizedBox(height: 10),
                        AuthTextField(
                          hinttext: 'Password',
                          prefixIcon: Icon(Icons.lock_outline),
                          isPassword: true,
                          color: Color(0xFF71C7E3),
                          isNumber: false,
                        )
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 20),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      FlatButton(
                          onPressed: () {
                            Navigator.pushNamed(context, "/seller_forget_pwd");
                          },
                          child: Text(
                            'Forget Password?',
                            style: TextStyle(color: Colors.grey[500]),
                          )),
                    ],
                  ),
                ),
                MyButton(
                    text: "Submit",
                    color: Color(0xFF003585),
                    onPressed: () {
                      Navigator.pushNamed(context, '/seller_home');
                    }),
                SizedBox(height: 20),
                Text('OR', style: TextStyle(fontWeight: FontWeight.bold)),
                SizedBox(height: 20),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    RawMaterialButton(
                        child: FaIcon(FontAwesomeIcons.facebookF,
                            size: 24, color: Colors.white),
                        fillColor: Colors.blue[900],
                        shape: CircleBorder(),
                        onPressed: () {}),
                    SizedBox(width: 20),
                    RawMaterialButton(
                        child: FaIcon(FontAwesomeIcons.googlePlusG,
                            size: 24, color: Colors.white),
                        fillColor: Colors.red,
                        shape: CircleBorder(),
                        onPressed: () {}),
                  ],
                ),
                SizedBox(height: 20),
                MyButton(
                  text: 'Guest Login',
                  color: Color(0xFF003585),
                  onPressed: () {
                    Navigator.pushNamed(context, '/seller_home');
                  },
                ),
                Spacer(),
                Row(
                  children: [
                    Expanded(
                      child: Divider(
                        color: Color(0xFF71C7E3),
                        height: 5.0,
                        thickness: 5,
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 10),
                Align(
                  alignment: Alignment.bottomCenter,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(bottom: 20.0),
                        child: RichText(
                          text: TextSpan(
                            children: <TextSpan>[
                              TextSpan(
                                  text: 'Not a Member Yet? ',
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontFamily: 'Dosis',
                                      color: Colors.grey,
                                      fontWeight: FontWeight.bold)),
                              TextSpan(
                                  text: 'Register with us',
                                  style: TextStyle(
                                      fontFamily: 'Dosis',
                                      fontSize: 18,
                                      color: Color(0xFF71C7E3),
                                      fontWeight: FontWeight.bold),
                                  recognizer: TapGestureRecognizer()
                                    ..onTap = () {
                                      Navigator.pushNamed(context, '/seller_signup');
                                    }),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),

    );
  }
}
