import 'package:flutter/material.dart';
import 'package:rent2buy/theme.dart';
import 'package:rent2buy/widgets/shared_widgets.dart';

class ItemAdded extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Theme(
      data: sellerTheme,
      child: Scaffold(
        appBar: BaseAppBar(
          appBar: AppBar(),
          title: 'Item Added',
          isCentered: true,
          automaticallyImplyLeading: false,
          iconTheme: Color(0xFF003585),
          titleColor: Color(0xFF003585),
        ),
        body: Container(
          padding: const EdgeInsets.all(20.0),
          child:
              Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                Image.network(
                'https://techcrunch.com/wp-content/uploads/2020/02/fujifilm-x100v.jpg'),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text('\$45.59',
                    style: TextStyle(
                        color: Color(0xFF003585),
                        fontWeight: FontWeight.w600,
                        fontSize: 36)),
                    Text('Sell',
                    style: TextStyle(
                        color: Colors.green,
                        fontWeight: FontWeight.w600,
                        fontSize: 36))
              ],
            ),
                Text('Canon Camera',
                style: TextStyle(
                    color: Color(0xFF003585),
                    fontWeight: FontWeight.w600,
                    fontSize: 36)),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                  Text('Date Of Listing',
                    style: TextStyle(color: Color(0xFF003585), fontSize: 24)),
                  Text('26/11/2020',
                    style: TextStyle(color: Color(0xFF003585), fontSize: 24))
              ],
            ),
                SizedBox(
              height: 40,
            ),
               FlatButton(
                onPressed: () {},
                child: Text('Get Approval from Admin',
                    style: TextStyle(
                        color: Color(0xFF003585),
                        fontWeight: FontWeight.w600,
                        fontSize: 28)))
          ]),
        ),
      ),
    );
  }
}
