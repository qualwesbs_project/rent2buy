import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:rent2buy/theme.dart';
import 'package:rent2buy/widgets/shared_widgets.dart';

class SellerSignup extends StatefulWidget {
  @override
  _SellerSignupState createState() => _SellerSignupState();
}

class _SellerSignupState extends State<SellerSignup> {
  final _key = GlobalKey<FormState>();
  bool _value = true;

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: sellerTheme,
      child: Scaffold(
        body: SingleChildScrollView(
          child: Container(
            height: MediaQuery.of(context).size.height,
            child: Column(
              children: [
                Container(
                  height: 150.0,
                  color: Color(0xFF71C7E3),
                  child: Center(
                    child: Text(
                      "R2B",
                      style: TextStyle(
                          fontFamily: 'Rammetto One',
                          color: Color(0xFF003585),
                          fontSize: 24.0),
                    ),
                  ),
                ),
                SizedBox(height: 20),
                Text(
                  "Register With Us!",
                  style: TextStyle(fontSize: 40, fontWeight: FontWeight.w800),
                ),
                SizedBox(height: 20),
                Form(
                  key: _key,
                  child: Padding(
                    padding: EdgeInsets.only(left: 20, right: 20),
                    child: Column(
                      children: [
                        AuthTextField(
                          hinttext: 'Full Name',
                          isNumber: false,
                          prefixIcon: Icon(Icons.person_outline),
                          color: Color(0xFF71C7E3),
                          isPassword: false,
                        ),
                        SizedBox(height: 10),
                        AuthTextField(
                          hinttext: 'Email Address',
                          isNumber: false,
                          prefixIcon: Icon(Icons.email_outlined),
                          color: Color(0xFF71C7E3),
                          isPassword: false,
                        ),
                        SizedBox(height: 10),
                        AuthTextField(
                            hinttext: 'Phone Number',
                            prefixIcon: Icon(Icons.smartphone),
                            color: Color(0xFF71C7E3),
                            isPassword: false,
                            isNumber: true),
                        SizedBox(height: 10),
                        AuthTextField(
                          hinttext: 'Password',
                          isNumber: false,
                          prefixIcon: Icon(Icons.lock_outline),
                          color: Color(0xFF71C7E3),
                          isPassword: true,
                        ),
                        SizedBox(height: 10),
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(left: 20),
                  child: Row(
                    children: [
                      Checkbox(
                        value: _value,
                        onChanged: (bool newValue) {
                          setState(() {
                            _value = newValue;
                          });
                        },
                      ),
                      SizedBox(width: 15),
                      Text('Agree with our Terms and Conditions.',
                          style:
                              TextStyle(fontSize: 14, color: Colors.black87)),
                    ],
                  ),
                ),
                SizedBox(height: 20),
                MyButton(
                  text: "Register",
                  onPressed: () {},
                  color: Color(0xFF003585),
                ),
                SizedBox(height: 20),
                Text('OR', style: TextStyle(fontWeight: FontWeight.bold)),
                SizedBox(height: 20),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    RawMaterialButton(
                        child: FaIcon(FontAwesomeIcons.facebookF,
                            size: 24, color: Colors.white),
                        fillColor: Colors.blue[900],
                        shape: CircleBorder(),
                        onPressed: () {}),
                    SizedBox(width: 20),
                    RawMaterialButton(
                        child: FaIcon(FontAwesomeIcons.googlePlusG,
                            size: 24, color: Colors.white),
                        fillColor: Colors.red,
                        shape: CircleBorder(),
                        onPressed: () {}),
                  ],
                ),
                Spacer(),
                Row(
                  children: [
                    Expanded(
                      child: Divider(
                        color: Color(0xFF71C7E3),
                        height: 5.0,
                        thickness: 5,
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 10),
                Align(
                  alignment: Alignment.bottomCenter,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(bottom: 20.0),
                        child: RichText(
                          text: TextSpan(
                            children: <TextSpan>[
                              TextSpan(
                                  text: 'Already Have an Account? ',
                                  style: TextStyle(
                                      fontFamily: 'Dosis',
                                      fontSize: 18,
                                      color: Colors.grey,
                                      fontWeight: FontWeight.bold)),
                              TextSpan(
                                  text: 'Login Here',
                                  style: TextStyle(
                                      fontFamily: 'Dosis',
                                      color: Color(0xFF71C7E3),
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold),
                                  recognizer: TapGestureRecognizer()
                                    ..onTap = () {
                                      Navigator.pushNamed(
                                          context, '/seller_login');
                                    }),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
