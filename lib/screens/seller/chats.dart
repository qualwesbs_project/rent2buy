import 'package:flutter/material.dart';
import 'package:rent2buy/models/user_model.dart';
import 'package:rent2buy/theme.dart';
import 'package:rent2buy/widgets/shared_widgets.dart';

class SellerChats extends StatelessWidget {
  final List<User> users = [
    User(name: 'User 1',imageUrl: 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQlR_mhU1bn_sa7j_M5UeMmIbKmR3UI1evxNw&usqp=CAU',
        msg: 'HIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII',
        id: 1
    ),
    User(name: 'User 2',imageUrl: 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQlR_mhU1bn_sa7j_M5UeMmIbKmR3UI1evxNw&usqp=CAU',
        msg: 'HIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII',
        id: 1
    ),
    User(name: 'User 3',imageUrl: 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQlR_mhU1bn_sa7j_M5UeMmIbKmR3UI1evxNw&usqp=CAU',
        msg: 'HIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII',
        id: 1
    ),
  ];
  @override
  Widget build(BuildContext context) {
    return Theme(
      data: sellerTheme,
      child: Scaffold(
        appBar: BaseAppBar(
          title: 'Chats',
          automaticallyImplyLeading: true,
          titleColor: Color(0xFF003585),
          iconTheme: Color(0xFF003585),
          widgets: [
            IconButton(icon: Icon(Icons.delete), onPressed: (){})
          ],
          appBar: AppBar(),
        ),
        body: ListView.builder(
            itemCount: users.length,
            itemBuilder: (BuildContext context, int index) {
              return Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(12.0),
                    child: ListTile(
                      leading: CircleAvatar(child: Image.network(users[index].imageUrl,),radius: 30,backgroundColor: Colors.transparent,),
                      title: Text(
                        users[index].name,
                        style: TextStyle(fontSize: 24,fontWeight: FontWeight.w700),
                      ),
                      subtitle: Padding(
                        padding: const EdgeInsets.only(top:8.0),
                        child: Text(
                          users[index].msg,
                          style: TextStyle(fontSize: 16),
                          overflow: TextOverflow.ellipsis,
                        ),
                      ),

                      onTap: (){
                        Navigator.pushNamed(context, '/seller_chat_screen');
                      },
                    ),
                  ),
                  Divider(
                    color: Color(0xFF71C7E3),
                    height: 5.0,
                    thickness: 3,
                  ),
                ],
              );
            }),
      ),
    );
  }
}
