import 'package:flutter/material.dart';
import 'package:rent2buy/theme.dart';
import 'package:rent2buy/widgets/shared_widgets.dart';

class SellerNotifications extends StatefulWidget {
  @override
  _SellerNotificationsState createState() => _SellerNotificationsState();
}

class _SellerNotificationsState extends State<SellerNotifications> {
  final List<String> _notifications = ['You Received Payment','You Accepted the Order', 'You Have new Order', 'You Have new Order', 'Bank Account Added','Add Bank Account','Add Listing','Account Verified','Seller Account Created '];

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: sellerTheme,
      child: Scaffold(
        appBar: BaseAppBar(
          appBar: AppBar(),
          automaticallyImplyLeading: true,
          title: 'Notifications',
          titleColor: Color(0xFF003585),
          iconTheme: Color(0xFF003585),
        ),
        body: Container(
          child: ListView.builder(
              itemCount: _notifications.length,
              itemBuilder: (BuildContext context, int index) {
                return Column(
                  children: [
                    ListTile(
                      contentPadding: EdgeInsets.symmetric(horizontal: 50),
                      title: Text(
                        _notifications[index],
                        style: TextStyle(fontWeight: FontWeight.w600, fontSize: 20),
                      ),
                    ),
                    Divider(
                      color: Color(0xFF71C7E3),
                      height: 5.0,
                      thickness: 3,
                    ),
                  ],
                );
              }),
        ),
      ),
    );
  }
}
