import 'package:flutter/material.dart';
import 'package:rent2buy/screens/seller/Inactive_listing.dart';
import 'package:rent2buy/screens/seller/active_listing.dart';
import 'package:rent2buy/theme.dart';
import 'package:rent2buy/widgets/shared_widgets.dart';

class AllListing extends StatefulWidget {
  @override
  _AllListingState createState() => _AllListingState();
}

class _AllListingState extends State<AllListing> {
  Color activeButtonColor;
  Color inactiveButtonColor = Color(0xFFE8F5FA);
  int elevation;
  int index = 0;

  List<Widget> listings = [
    ActiveListing(),
    InactiveListing()
  ];

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: sellerTheme,
      child: Scaffold(
        appBar: BaseAppBar(
          appBar: AppBar(),
          title: 'All Listing',
          isCentered: true,
          automaticallyImplyLeading: true,
          iconTheme: Color(0xFF003585),
          titleColor: Color(0xFF003585),
        ),
        body: listings[index],
        bottomNavigationBar: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            RaisedButton(
              elevation: 0,
              color: activeButtonColor,
              child: Text('Active',style: TextStyle(fontSize: 16, fontWeight: FontWeight.w800,color:Color(0xFF003585))),
                onPressed: () {
                  activeButtonColor = Color(0xFF71C7E3);
                  inactiveButtonColor = Color(0xFFE8F5FA);
                  index = 0;
                  setState(() {
                  });
                }
                ),
            SizedBox(width: 10),
            RaisedButton(
              elevation: 0,
              color: inactiveButtonColor,
                child: Text('Inactive',style: TextStyle(fontSize: 16, fontWeight: FontWeight.w800,color:Color(0xFF003585))),
                onPressed: () {
                  activeButtonColor = Color(0xFFE8F5FA);
                  inactiveButtonColor = Color(0xFF71C7E3);
                  index = 1;
                  setState(() {
                  });
                }
            ),
          ],
        ),
      ),
    );
  }
}
