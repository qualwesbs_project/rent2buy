import 'package:flutter/material.dart';
import 'package:rent2buy/theme.dart';
import 'package:rent2buy/widgets/shared_widgets.dart';

class SellerSettings extends StatelessWidget {

  final _bigFont = const TextStyle(
      fontSize: 20, fontWeight: FontWeight.w700);
  final padding = const EdgeInsets.symmetric(horizontal: 50,vertical: 12);
  @override
  Widget build(BuildContext context) {
    return Theme(
      data: sellerTheme,
      child: Scaffold(
        appBar: BaseAppBar(
          appBar: AppBar(),
          title: 'Seller Settings',
          isCentered: true,
          automaticallyImplyLeading: true,
          iconTheme: Color(0xFF003585),
          titleColor: Color(0xFF003585),
        ),
        body: ListView(
          children: [
            ListTile(
              contentPadding: padding,
              leading: Icon(Icons.cloud_upload_outlined,color: Colors.black,size: 32,),
              title: Text('Upload New Items',style: _bigFont,),
            ),
            CustomDivider(),
            ListTile(
              contentPadding: padding,
              leading:Image.asset('assets/analytics.png',height: 34,),
              title: Text('Obtain Report of sold Items',style: _bigFont,),
            ),
            CustomDivider(),
            ListTile(
              contentPadding: padding,
              leading: Icon(Icons.account_balance_outlined,color: Colors.black,size: 32,),
              title: Text('Remove Bank Account',style: _bigFont,),
              onTap: (){
                Navigator.pushNamed(context, '/bank_account');
              },
            ),
            CustomDivider(),
            ListTile(
              contentPadding: padding,
              leading: Icon(Icons.lock_open_outlined,color: Colors.black,size: 32,),
              title: Text('Reset Password',style: _bigFont,),
            ),
            CustomDivider(),
            ListTile(
              contentPadding: padding,
              leading: Icon(Icons.person_remove_alt_1_outlined,color: Colors.black,size: 32,),
              title: Text('Remove Account',style: _bigFont,),
            ),
            CustomDivider(),
          ],
        ),
      ),
    );
  }
}

