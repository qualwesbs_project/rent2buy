import 'package:flutter/material.dart';
import 'package:rent2buy/theme.dart';
import 'package:rent2buy/widgets/shared_widgets.dart';
class SellerResetPassword extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Theme(
      data: sellerTheme,
      child: Scaffold(
        appBar: appBar(Color(0xFF003585)),
        body: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 40),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: 60),
                Padding(
                  padding: EdgeInsets.only(left: 12.0),
                  child: Text('Reset Password',
                      style: TextStyle(
                          fontFamily: 'Dosis',
                          fontWeight: FontWeight.w700,
                          fontSize: 24)),
                ),
                SizedBox(height: 40),
                CustomTextField(
                    hinttext: 'Current Password',
                    borderColor: Color(0xFF003585),
                    isPassword: true,
                    isNumber: false), //TODO
                SizedBox(height: 40),
                CustomTextField(
                    hinttext: 'New Password',
                    borderColor: Color(0xFF003585),
                    isPassword: true,
                    isNumber: false),
                SizedBox(height: 40),
                CustomTextField(
                    hinttext: 'Confirm Password',
                    borderColor: Color(0xFF003585),
                    isPassword: true,
                    isNumber: false),
                SizedBox(height: 60),
                Center(
                    child: MyButton(
                        text: 'Reset',
                        color: Color(0xFF003585),
                        onPressed: () {
                        }))
              ],
            ),
          ),
        ),
      ),
    );
  }
}
