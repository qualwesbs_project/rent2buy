import 'package:flutter/material.dart';
import 'package:rent2buy/theme.dart';
import 'package:rent2buy/widgets/shared_widgets.dart';
import '../../data.dart';

class SellerDispute extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Theme(
      data: sellerTheme,
        child: Scaffold(
          appBar: BaseAppBar(
            appBar: AppBar(),
            title: 'Dispute',
            isCentered: true,
            automaticallyImplyLeading: true,
            iconTheme: Color(0xFF003585),
            titleColor: Color(0xFF003585),
          ),
          body: ListView.builder(
              itemCount: disputeList.length,
              itemBuilder: (BuildContext context, int index) {
                return Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(12.0),
                      child: ListTile(
                        leading: Image.network(disputeList[index].imageUrl,fit: BoxFit.fitHeight,width: 100,height: 100,),
                        title: Text(
                          disputeList[index].name,
                          style: TextStyle(fontSize: 24),
                        ),
                        subtitle: Padding(
                          padding: const EdgeInsets.only(top:8.0),
                          child: Text(
                            disputeList[index].date,
                            style: TextStyle(fontSize: 16),
                          ),
                        ),
                        trailing: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              '\u0024' + disputeList[index].price.toString(),
                              style:
                              TextStyle(color: Color(0xFF97080E), fontSize: 24),
                            ),
                            Text(
                              disputeList[index].status,
                              style: TextStyle(color: Colors.green[600],fontSize: 16),
                            )
                          ],
                        ),
                        onTap: (){
                          Navigator.pushNamed(context, '/seller_product_detail');
                        },
                      ),
                    ),
                    CustomDivider()
                  ],
                );
              }),
        )
    );
  }
}
