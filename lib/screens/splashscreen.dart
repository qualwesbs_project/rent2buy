import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {

  @override
  void initState() {
    User user = FirebaseAuth.instance.currentUser;
    super.initState();
    Timer(Duration(seconds: 3), () => {
      if(user == null){
        Navigator.of(context).pushNamedAndRemoveUntil('/login', (Route<dynamic> route) => false)
      }else{
        Navigator.of(context).pushNamedAndRemoveUntil('/home', (Route<dynamic> route) => false)
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Column(
          children: [
            Spacer(),
            Center(
              child: Text(
                "Rent 2 Buy",
                style: TextStyle(
                    fontFamily: 'Interval Sans Pro',
                    color: Color(0xFF97080E),
                    fontWeight: FontWeight.bold,
                    fontSize: 50.0,
                    shadows: [Shadow(color: Colors.grey,blurRadius: 1.0,offset: Offset(-2.0, 2.0))]
                ),
              ),
            ),
            Spacer(),
            Padding(
                padding: EdgeInsets.only(bottom: 30.0),
                child: CupertinoActivityIndicator(radius: 16 ,animating: true)
            ),
          ],
        ),
      ),
    );
  }
}

