import 'user_model.dart';

class Message{
  final User sender;
  final String text;

  Message({this.sender,this.text});
}

List<Message> chats = [
  Message(sender: user1, text: 'Hii'),
  Message(sender: user2, text: 'Hii'),
  Message(sender: user3, text: 'Hii'),
];


List<Message> messages = [
  Message(sender: user1,text: 'HIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII'),
  Message(sender: user2,text: 'HIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII'),
  Message(sender: user3,text: 'HIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII')
];