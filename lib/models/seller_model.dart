class SellerModel{
  String name;
  String location;
  String imageUrl;
  double rating;

  SellerModel({this.name,this.imageUrl,this.location,this.rating});
}