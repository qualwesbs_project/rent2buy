
class ProductModel{
  String name;
  String description;
  String imageUrl;
  var date;
  double price;
  String status;
  String keyword;
  double radius;
  String sellType;
  String avaliability;
  ProductModel(this.name,this.description,this.imageUrl,this.date,this.price,this.status,this.keyword,this.radius,this.sellType,this.avaliability);
}

